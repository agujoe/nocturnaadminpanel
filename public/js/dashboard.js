//draw bar chart for user and frequency of post on the dashboard
function getPostDataFrequency(){
    let request = new XMLHttpRequest();
    let arr = [];

    request.onreadystatechange = function () {
        if(request .readyState === 4 && request.status === 200){
            arr = JSON.parse(request.response);
            post = Object.values(arr);
            month = Object.keys(arr);

            getPostData(post, month);
        }

        function  getPostData(post, month) {
            drawPostDataFrequency(post, month);
        }
    };

    //making false the third parameter to this function makes it synchronous
    request.open("GET", "http://localhost/adminpanel/post/frequency");
    request.send();
}

function drawPostDataFrequency(post, month){
    console.log(month);
    let dateArray = [];
    Date.prototype.getMonthName = function() {
        let months = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];

        return months[this.getMonth()];
    };
    for (i = 0; i < month.length; i++){
        let date = new Date(month[i]);
        let year = date.getFullYear();
        let month_name = date.getMonthName(month[i]);
        let day_string = month_name + " " + year;
        dateArray.push(day_string)
    }

    let date = new Date(month);
    console.log(date);

    //console.log(post, month);
    let postChart = document.getElementById('postFrequency');
    let myChart = new Chart(postChart, {
        type: 'bar',
        data: {
            labels: dateArray,
            datasets: [{
                label: 'Number of Posts',
                data: post,
                height: 400,
                backgroundColor: [
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)'
                ],
                borderColor: [
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                    'rgba(102, 0, 102, 1)'
                ],
                borderWidth: 1
            }]
        },

        options: {
            scales: {
                yAxes: [{
                    categorySpacing: 0,
                    ticks: {
                        beginAtZero: true
                    },
                }],

                xAxes: [{
                    barThickness: 35,
                    categorySpacing: 0,
                    ticks: {
                        beginAtZero: true
                    },
                }]
            }
        }
    });
}

getPostDataFrequency();



//draw chart for most used device
function getDevice(){
    let request = new XMLHttpRequest();
    let devices = [];

    request.onreadystatechange = function () {
        if(request .readyState === 4 && request.status === 200){
            devices = JSON.parse(request.response);
            getResponseData(devices);
        }
    };

    function getResponseData(devices){
        let andoid_device_count = devices.android;
        let ios_device_count = devices.ios;

        console.log(andoid_device_count, ios_device_count);
        drawDeviceChart(andoid_device_count, ios_device_count);
    }


    //making false the third parameter to this function makes it synchronous
    request.open("GET", "http://localhost/adminpanel/admin/devices");
    request.send();
}

function drawDeviceChart(android_device_count, ios_device_count){
    let canvas = document.getElementById("devices").getContext('2d');
    let deviceChart = new Chart(canvas, {
        type: 'pie',
        data:{
            labels: ["Android Users", "iOS Users"],
            datasets: [{
                label: "Mobile Devices",
                data: [android_device_count, ios_device_count],
                backgroundColor: [
                    'rgba(0, 102, 102, 2)',
                    'rgba(102, 0, 102, 2)',
                ],
                borderColor: [
                    'rgba(0, 102, 102, 1)',
                    'rgba(102, 0, 102, 1)',
                ],
                borderWidth : 1
            }]
        },
        options: {
            responsive:true,
            fontsize: 24,
        }
    });
}

getDevice();



//draw chart for Normal Users and Place Owners
function getNormalUserAgainstOwnerUser(){
    let request = new XMLHttpRequest();
    let all_users = [];

    request.onreadystatechange = function () {
        if(request .readyState === 4 && request.status === 200){
            all_users = JSON.parse(request.response);
            console.log(all_users);
            getResponseData(all_users);
        }
    };

    function getResponseData(users){
        console.log(users);
        let normaluser_count = users.normaluser;
        let owneruser_count = users.owneruser;

        console.log(normaluser_count, owneruser_count);
        drawNormalUserAgainstOwnerUserRatio(normaluser_count, owneruser_count);
    }


    //making false the third parameter to this function makes it synchronous
    request.open("GET", "http://localhost/adminpanel/admin/user/ratio");
    request.send();
}

function drawNormalUserAgainstOwnerUserRatio(normaluser_count, owneruser_count){
    let canvas = document.getElementById("user_ratio").getContext('2d');
    let deviceChart = new Chart(canvas, {
        type: 'pie',
        data:{
            labels: ["Normal Users", "Place Owners"],
            datasets: [{
                label: "Users Ratio",
                data: [normaluser_count, owneruser_count],
                backgroundColor: [
                    'rgba(232, 0, 13, 2)',
                    'rgba(244, 208, 63, 2)'
                ],
                borderColor: [
                    'rgba(34, 153, 84, 1)',
                    'rgba(102, 0, 102, 1)',
                ],
                borderWidth : 1
            }]
        },
        options: {
            responsive:true,
            fontsize: 24,
        }
    });
}

getNormalUserAgainstOwnerUser();



//draw chart for Purchase Status
function getPurchaseStatusRatio(){
    let request = new XMLHttpRequest();
    let purchases = [];

    request.onreadystatechange = function () {
        if(request .readyState === 4 && request.status === 200){
            purchases = JSON.parse(request.response);
            console.log(purchases);
            getResponseData(purchases);
        }
    };

    function getResponseData(purchases){
        console.log(purchases);
        let purchased_product = purchases.purchased;
        let pending_product = purchases.pending;

        console.log(purchased_product, pending_product);
        drawPurchaseStatusRatio(purchased_product, pending_product);
    }


    //making false the third parameter to this function makes it synchronous
    request.open("GET", "http://localhost/adminpanel/admin/purchase/ratio");
    request.send();
}

function drawPurchaseStatusRatio(purchased_product, pending_product){
    let canvas = document.getElementById("purchase_ratio").getContext('2d');
    let deviceChart = new Chart(canvas, {
        type: 'pie',
        data:{
            labels: ["Purchased Products", "Pending Products"],
            datasets: [{
                label: "Purchase Ratio",
                data: [purchased_product, pending_product],
                backgroundColor: [
                    'rgba(35, 155, 86, 2)',
                    'rgba(46, 64, 83, 2)'
                ],
                borderColor: [
                    'rgba(34, 153, 84, 1)',
                    'rgba(102, 0, 102, 1)',
                ],
                borderWidth : 1
            }]
        },
        options: {
            responsive:true,
            fontsize: 24,
        }
    });
}

getPurchaseStatusRatio();
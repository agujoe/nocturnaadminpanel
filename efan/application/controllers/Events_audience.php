<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Events_audience extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Events_audience_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'events_audience?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'events_audience?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'events_audience/';
            $config['first_url'] = base_url() . 'events_audience/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Events_audience_model->total_rows($q);
        $events_audience = $this->Events_audience_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'events_audience_data' => $events_audience,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('events_audience/events_audience_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Events_audience_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'event_id' => $row->event_id,
		'user_id' => $row->user_id,
		'invite_id' => $row->invite_id,
		'status' => $row->status,
		'created_at' => $row->created_at,
	    );
            $this->load->view('events_audience/events_audience_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('events_audience'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('events_audience/create_action'),
	    'id' => set_value('id'),
	    'event_id' => set_value('event_id'),
	    'user_id' => set_value('user_id'),
	    'invite_id' => set_value('invite_id'),
	    'status' => set_value('status'),
	    'created_at' => set_value('created_at'),
	);
        $this->load->view('events_audience/events_audience_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'event_id' => $this->input->post('event_id',TRUE),
		'user_id' => $this->input->post('user_id',TRUE),
		'invite_id' => $this->input->post('invite_id',TRUE),
		'status' => $this->input->post('status',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
	    );

            $this->Events_audience_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('events_audience'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Events_audience_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('events_audience/update_action'),
		'id' => set_value('id', $row->id),
		'event_id' => set_value('event_id', $row->event_id),
		'user_id' => set_value('user_id', $row->user_id),
		'invite_id' => set_value('invite_id', $row->invite_id),
		'status' => set_value('status', $row->status),
		'created_at' => set_value('created_at', $row->created_at),
	    );
            $this->load->view('events_audience/events_audience_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('events_audience'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'event_id' => $this->input->post('event_id',TRUE),
		'user_id' => $this->input->post('user_id',TRUE),
		'invite_id' => $this->input->post('invite_id',TRUE),
		'status' => $this->input->post('status',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
	    );

            $this->Events_audience_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('events_audience'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Events_audience_model->get_by_id($id);

        if ($row) {
            $this->Events_audience_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('events_audience'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('events_audience'));
        }
    }

    public function _rules() { 
	$this->form_validation->set_rules('event_id', 'event id', 'trim|required');
	$this->form_validation->set_rules('user_id', 'user id', 'trim|required');
	$this->form_validation->set_rules('invite_id', 'invite id', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');
	$this->form_validation->set_rules('created_at', 'created at', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "events_audience.xls";
        $judul = "events_audience";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Event Id");
	xlsWriteLabel($tablehead, $kolomhead++, "User Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Invite Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");
	xlsWriteLabel($tablehead, $kolomhead++, "Created At");

	foreach ($this->Events_audience_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->event_id);
	    xlsWriteNumber($tablebody, $kolombody++, $data->user_id);
	    xlsWriteNumber($tablebody, $kolombody++, $data->invite_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_at);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=events_audience.doc");

        $data = array(
            'events_audience_data' => $this->Events_audience_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('events_audience/events_audience_doc',$data);
    }

}

/* End of file Events_audience.php */
/* Location: ./application/controllers/Events_audience.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-10-05 10:02:20 */
/* http://harviacode.com */
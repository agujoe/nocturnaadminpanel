<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Posts extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Posts_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'posts?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'posts?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'posts/';
            $config['first_url'] = base_url() . 'posts/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Posts_model->total_rows($q);
        $posts = $this->Posts_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'posts_data' => $posts,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('posts/posts_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Posts_model->get_by_id($id);
        if ($row) {
            $data = array(
		'post_id' => $row->post_id,
		'user_id' => $row->user_id,
		'post_type' => $row->post_type,
		'status_type' => $row->status_type,
		'message' => $row->message,
		'created_at' => $row->created_at,
		'link' => $row->link,
	    );
            $this->load->view('posts/posts_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('posts'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('posts/create_action'),
	    'post_id' => set_value('post_id'),
	    'user_id' => set_value('user_id'),
	    'post_type' => set_value('post_type'),
	    'status_type' => set_value('status_type'),
	    'message' => set_value('message'),
	    'created_at' => set_value('created_at'),
	    'link' => set_value('link'),
	);
        $this->load->view('posts/posts_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'user_id' => $this->input->post('user_id',TRUE),
		'post_type' => $this->input->post('post_type',TRUE),
		'status_type' => $this->input->post('status_type',TRUE),
		'message' => $this->input->post('message',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'link' => $this->input->post('link',TRUE),
	    );

            $this->Posts_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('posts'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Posts_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('posts/update_action'),
		'post_id' => set_value('post_id', $row->post_id),
		'user_id' => set_value('user_id', $row->user_id),
		'post_type' => set_value('post_type', $row->post_type),
		'status_type' => set_value('status_type', $row->status_type),
		'message' => set_value('message', $row->message),
		'created_at' => set_value('created_at', $row->created_at),
		'link' => set_value('link', $row->link),
	    );
            $this->load->view('posts/posts_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('posts'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('post_id', TRUE));
        } else {
            $data = array(
		'user_id' => $this->input->post('user_id',TRUE),
		'post_type' => $this->input->post('post_type',TRUE),
		'status_type' => $this->input->post('status_type',TRUE),
		'message' => $this->input->post('message',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'link' => $this->input->post('link',TRUE),
	    );

            $this->Posts_model->update($this->input->post('post_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('posts'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Posts_model->get_by_id($id);

        if ($row) {
            $this->Posts_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('posts'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('posts'));
        }
    }

    public function _rules() { 
	$this->form_validation->set_rules('user_id', 'user id', 'trim|required');
	$this->form_validation->set_rules('post_type', 'post type', 'trim|required');
	$this->form_validation->set_rules('status_type', 'status type', 'trim|required');
	$this->form_validation->set_rules('message', 'message', 'trim|required');
	$this->form_validation->set_rules('created_at', 'created at', 'trim|required');
	$this->form_validation->set_rules('link', 'link', 'trim|required');

	$this->form_validation->set_rules('post_id', 'post_id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "posts.xls";
        $judul = "posts";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "User Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Post Type");
	xlsWriteLabel($tablehead, $kolomhead++, "Status Type");
	xlsWriteLabel($tablehead, $kolomhead++, "Message");
	xlsWriteLabel($tablehead, $kolomhead++, "Created At");
	xlsWriteLabel($tablehead, $kolomhead++, "Link");

	foreach ($this->Posts_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->user_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->post_type);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status_type);
	    xlsWriteLabel($tablebody, $kolombody++, $data->message);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_at);
	    xlsWriteLabel($tablebody, $kolombody++, $data->link);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=posts.doc");

        $data = array(
            'posts_data' => $this->Posts_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('posts/posts_doc',$data);
    }

}

/* End of file Posts.php */
/* Location: ./application/controllers/Posts.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-10-05 10:02:20 */
/* http://harviacode.com */
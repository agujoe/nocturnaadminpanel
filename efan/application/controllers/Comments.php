<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Comments extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Comments_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'comments?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'comments?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'comments/';
            $config['first_url'] = base_url() . 'comments/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Comments_model->total_rows($q);
        $comments = $this->Comments_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'comments_data' => $comments,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('comments/comments_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Comments_model->get_by_id($id);
        if ($row) {
            $data = array(
		'post_id' => $row->post_id,
		'user_id' => $row->user_id,
		'comment' => $row->comment,
		'created_at' => $row->created_at,
	    );
            $this->load->view('comments/comments_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('comments'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('comments/create_action'),
	    'post_id' => set_value('post_id'),
	    'user_id' => set_value('user_id'),
	    'comment' => set_value('comment'),
	    'created_at' => set_value('created_at'),
	);
        $this->load->view('comments/comments_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'post_id' => $this->input->post('post_id',TRUE),
		'user_id' => $this->input->post('user_id',TRUE),
		'comment' => $this->input->post('comment',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
	    );

            $this->Comments_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('comments'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Comments_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('comments/update_action'),
		'post_id' => set_value('post_id', $row->post_id),
		'user_id' => set_value('user_id', $row->user_id),
		'comment' => set_value('comment', $row->comment),
		'created_at' => set_value('created_at', $row->created_at),
	    );
            $this->load->view('comments/comments_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('comments'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('', TRUE));
        } else {
            $data = array(
		'post_id' => $this->input->post('post_id',TRUE),
		'user_id' => $this->input->post('user_id',TRUE),
		'comment' => $this->input->post('comment',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
	    );

            $this->Comments_model->update($this->input->post('', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('comments'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Comments_model->get_by_id($id);

        if ($row) {
            $this->Comments_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('comments'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('comments'));
        }
    }

    public function _rules() { 
	$this->form_validation->set_rules('post_id', 'post id', 'trim|required');
	$this->form_validation->set_rules('user_id', 'user id', 'trim|required');
	$this->form_validation->set_rules('comment', 'comment', 'trim|required');
	$this->form_validation->set_rules('created_at', 'created at', 'trim|required');

	$this->form_validation->set_rules('', '', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "comments.xls";
        $judul = "comments";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Post Id");
	xlsWriteLabel($tablehead, $kolomhead++, "User Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Comment");
	xlsWriteLabel($tablehead, $kolomhead++, "Created At");

	foreach ($this->Comments_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->post_id);
	    xlsWriteNumber($tablebody, $kolombody++, $data->user_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->comment);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_at);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=comments.doc");

        $data = array(
            'comments_data' => $this->Comments_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('comments/comments_doc',$data);
    }

}

/* End of file Comments.php */
/* Location: ./application/controllers/Comments.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-10-05 10:02:19 */
/* http://harviacode.com */
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Events extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Events_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'events?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'events?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'events/';
            $config['first_url'] = base_url() . 'events/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Events_model->total_rows($q);
        $events = $this->Events_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'events_data' => $events,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('events/events_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Events_model->get_by_id($id);
        if ($row) {
            $data = array(
		'event_id' => $row->event_id,
		'user_id' => $row->user_id,
		'title' => $row->title,
		'description' => $row->description,
		'start_time' => $row->start_time,
		'end_time' => $row->end_time,
		'start_date' => $row->start_date,
		'end_date' => $row->end_date,
		'can_invite' => $row->can_invite,
		'location' => $row->location,
		'event_pic' => $row->event_pic,
		'visitors' => $row->visitors,
		'event_type' => $row->event_type,
		'created_at' => $row->created_at,
	    );
            $this->load->view('events/events_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('events'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('events/create_action'),
	    'event_id' => set_value('event_id'),
	    'user_id' => set_value('user_id'),
	    'title' => set_value('title'),
	    'description' => set_value('description'),
	    'start_time' => set_value('start_time'),
	    'end_time' => set_value('end_time'),
	    'start_date' => set_value('start_date'),
	    'end_date' => set_value('end_date'),
	    'can_invite' => set_value('can_invite'),
	    'location' => set_value('location'),
	    'event_pic' => set_value('event_pic'),
	    'visitors' => set_value('visitors'),
	    'event_type' => set_value('event_type'),
	    'created_at' => set_value('created_at'),
	);
        $this->load->view('events/events_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'user_id' => $this->input->post('user_id',TRUE),
		'title' => $this->input->post('title',TRUE),
		'description' => $this->input->post('description',TRUE),
		'start_time' => $this->input->post('start_time',TRUE),
		'end_time' => $this->input->post('end_time',TRUE),
		'start_date' => $this->input->post('start_date',TRUE),
		'end_date' => $this->input->post('end_date',TRUE),
		'can_invite' => $this->input->post('can_invite',TRUE),
		'location' => $this->input->post('location',TRUE),
		'event_pic' => $this->input->post('event_pic',TRUE),
		'visitors' => $this->input->post('visitors',TRUE),
		'event_type' => $this->input->post('event_type',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
	    );

            $this->Events_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('events'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Events_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('events/update_action'),
		'event_id' => set_value('event_id', $row->event_id),
		'user_id' => set_value('user_id', $row->user_id),
		'title' => set_value('title', $row->title),
		'description' => set_value('description', $row->description),
		'start_time' => set_value('start_time', $row->start_time),
		'end_time' => set_value('end_time', $row->end_time),
		'start_date' => set_value('start_date', $row->start_date),
		'end_date' => set_value('end_date', $row->end_date),
		'can_invite' => set_value('can_invite', $row->can_invite),
		'location' => set_value('location', $row->location),
		'event_pic' => set_value('event_pic', $row->event_pic),
		'visitors' => set_value('visitors', $row->visitors),
		'event_type' => set_value('event_type', $row->event_type),
		'created_at' => set_value('created_at', $row->created_at),
	    );
            $this->load->view('events/events_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('events'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('event_id', TRUE));
        } else {
            $data = array(
		'user_id' => $this->input->post('user_id',TRUE),
		'title' => $this->input->post('title',TRUE),
		'description' => $this->input->post('description',TRUE),
		'start_time' => $this->input->post('start_time',TRUE),
		'end_time' => $this->input->post('end_time',TRUE),
		'start_date' => $this->input->post('start_date',TRUE),
		'end_date' => $this->input->post('end_date',TRUE),
		'can_invite' => $this->input->post('can_invite',TRUE),
		'location' => $this->input->post('location',TRUE),
		'event_pic' => $this->input->post('event_pic',TRUE),
		'visitors' => $this->input->post('visitors',TRUE),
		'event_type' => $this->input->post('event_type',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
	    );

            $this->Events_model->update($this->input->post('event_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('events'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Events_model->get_by_id($id);

        if ($row) {
            $this->Events_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('events'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('events'));
        }
    }

    public function _rules() { 
	$this->form_validation->set_rules('user_id', 'user id', 'trim|required');
	$this->form_validation->set_rules('title', 'title', 'trim|required');
	$this->form_validation->set_rules('description', 'description', 'trim|required');
	$this->form_validation->set_rules('start_time', 'start time', 'trim|required');
	$this->form_validation->set_rules('end_time', 'end time', 'trim|required');
	$this->form_validation->set_rules('start_date', 'start date', 'trim|required');
	$this->form_validation->set_rules('end_date', 'end date', 'trim|required');
	$this->form_validation->set_rules('can_invite', 'can invite', 'trim|required');
	$this->form_validation->set_rules('location', 'location', 'trim|required');
	$this->form_validation->set_rules('event_pic', 'event pic', 'trim|required');
	$this->form_validation->set_rules('visitors', 'visitors', 'trim|required');
	$this->form_validation->set_rules('event_type', 'event type', 'trim|required');
	$this->form_validation->set_rules('created_at', 'created at', 'trim|required');

	$this->form_validation->set_rules('event_id', 'event_id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "events.xls";
        $judul = "events";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "User Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Title");
	xlsWriteLabel($tablehead, $kolomhead++, "Description");
	xlsWriteLabel($tablehead, $kolomhead++, "Start Time");
	xlsWriteLabel($tablehead, $kolomhead++, "End Time");
	xlsWriteLabel($tablehead, $kolomhead++, "Start Date");
	xlsWriteLabel($tablehead, $kolomhead++, "End Date");
	xlsWriteLabel($tablehead, $kolomhead++, "Can Invite");
	xlsWriteLabel($tablehead, $kolomhead++, "Location");
	xlsWriteLabel($tablehead, $kolomhead++, "Event Pic");
	xlsWriteLabel($tablehead, $kolomhead++, "Visitors");
	xlsWriteLabel($tablehead, $kolomhead++, "Event Type");
	xlsWriteLabel($tablehead, $kolomhead++, "Created At");

	foreach ($this->Events_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->user_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->title);
	    xlsWriteLabel($tablebody, $kolombody++, $data->description);
	    xlsWriteLabel($tablebody, $kolombody++, $data->start_time);
	    xlsWriteLabel($tablebody, $kolombody++, $data->end_time);
	    xlsWriteLabel($tablebody, $kolombody++, $data->start_date);
	    xlsWriteLabel($tablebody, $kolombody++, $data->end_date);
	    xlsWriteLabel($tablebody, $kolombody++, $data->can_invite);
	    xlsWriteLabel($tablebody, $kolombody++, $data->location);
	    xlsWriteLabel($tablebody, $kolombody++, $data->event_pic);
	    xlsWriteNumber($tablebody, $kolombody++, $data->visitors);
	    xlsWriteLabel($tablebody, $kolombody++, $data->event_type);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_at);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=events.doc");

        $data = array(
            'events_data' => $this->Events_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('events/events_doc',$data);
    }

}

/* End of file Events.php */
/* Location: ./application/controllers/Events.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-10-05 10:02:19 */
/* http://harviacode.com */
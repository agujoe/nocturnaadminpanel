<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fanbase extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Fanbase_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'fanbase?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'fanbase?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'fanbase/';
            $config['first_url'] = base_url() . 'fanbase/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Fanbase_model->total_rows($q);
        $fanbase = $this->Fanbase_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'fanbase_data' => $fanbase,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('fanbase/fanbase_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Fanbase_model->get_by_id($id);
        if ($row) {
            $data = array(
		'user_id' => $row->user_id,
		'fan_id' => $row->fan_id,
		'created_at' => $row->created_at,
	    );
            $this->load->view('fanbase/fanbase_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('fanbase'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('fanbase/create_action'),
	    'user_id' => set_value('user_id'),
	    'fan_id' => set_value('fan_id'),
	    'created_at' => set_value('created_at'),
	);
        $this->load->view('fanbase/fanbase_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'user_id' => $this->input->post('user_id',TRUE),
		'fan_id' => $this->input->post('fan_id',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
	    );

            $this->Fanbase_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('fanbase'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Fanbase_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('fanbase/update_action'),
		'user_id' => set_value('user_id', $row->user_id),
		'fan_id' => set_value('fan_id', $row->fan_id),
		'created_at' => set_value('created_at', $row->created_at),
	    );
            $this->load->view('fanbase/fanbase_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('fanbase'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('', TRUE));
        } else {
            $data = array(
		'user_id' => $this->input->post('user_id',TRUE),
		'fan_id' => $this->input->post('fan_id',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
	    );

            $this->Fanbase_model->update($this->input->post('', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('fanbase'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Fanbase_model->get_by_id($id);

        if ($row) {
            $this->Fanbase_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('fanbase'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('fanbase'));
        }
    }

    public function _rules() { 
	$this->form_validation->set_rules('user_id', 'user id', 'trim|required');
	$this->form_validation->set_rules('fan_id', 'fan id', 'trim|required');
	$this->form_validation->set_rules('created_at', 'created at', 'trim|required');

	$this->form_validation->set_rules('', '', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "fanbase.xls";
        $judul = "fanbase";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "User Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Fan Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Created At");

	foreach ($this->Fanbase_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->user_id);
	    xlsWriteNumber($tablebody, $kolombody++, $data->fan_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_at);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=fanbase.doc");

        $data = array(
            'fanbase_data' => $this->Fanbase_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('fanbase/fanbase_doc',$data);
    }

}

/* End of file Fanbase.php */
/* Location: ./application/controllers/Fanbase.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-10-05 10:02:20 */
/* http://harviacode.com */
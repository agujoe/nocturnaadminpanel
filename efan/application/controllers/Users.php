<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'users?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'users?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'users/';
            $config['first_url'] = base_url() . 'users/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Users_model->total_rows($q);
        $users = $this->Users_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'users_data' => $users,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('users/users_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Users_model->get_by_id($id);
        if ($row) {
            $data = array(
		'user_id' => $row->user_id,
		'first_name' => $row->first_name,
		'last_name' => $row->last_name,
		'username' => $row->username,
		'password' => $row->password,
		'about_me' => $row->about_me,
		'relationship_status' => $row->relationship_status,
		'city' => $row->city,
		'profession' => $row->profession,
		'created_at' => $row->created_at,
		'device_token' => $row->device_token,
		'device_type' => $row->device_type,
	    );
            $this->load->view('users/users_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('users/create_action'),
	    'user_id' => set_value('user_id'),
	    'first_name' => set_value('first_name'),
	    'last_name' => set_value('last_name'),
	    'username' => set_value('username'),
	    'password' => set_value('password'),
	    'about_me' => set_value('about_me'),
	    'relationship_status' => set_value('relationship_status'),
	    'city' => set_value('city'),
	    'profession' => set_value('profession'),
	    'created_at' => set_value('created_at'),
	    'device_token' => set_value('device_token'),
	    'device_type' => set_value('device_type'),
	);
        $this->load->view('users/users_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'first_name' => $this->input->post('first_name',TRUE),
		'last_name' => $this->input->post('last_name',TRUE),
		'username' => $this->input->post('username',TRUE),
		'password' => $this->input->post('password',TRUE),
		'about_me' => $this->input->post('about_me',TRUE),
		'relationship_status' => $this->input->post('relationship_status',TRUE),
		'city' => $this->input->post('city',TRUE),
		'profession' => $this->input->post('profession',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'device_token' => $this->input->post('device_token',TRUE),
		'device_type' => $this->input->post('device_type',TRUE),
	    );

            $this->Users_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('users'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Users_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('users/update_action'),
		'user_id' => set_value('user_id', $row->user_id),
		'first_name' => set_value('first_name', $row->first_name),
		'last_name' => set_value('last_name', $row->last_name),
		'username' => set_value('username', $row->username),
		'password' => set_value('password', $row->password),
		'about_me' => set_value('about_me', $row->about_me),
		'relationship_status' => set_value('relationship_status', $row->relationship_status),
		'city' => set_value('city', $row->city),
		'profession' => set_value('profession', $row->profession),
		'created_at' => set_value('created_at', $row->created_at),
		'device_token' => set_value('device_token', $row->device_token),
		'device_type' => set_value('device_type', $row->device_type),
	    );
            $this->load->view('users/users_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('user_id', TRUE));
        } else {
            $data = array(
		'first_name' => $this->input->post('first_name',TRUE),
		'last_name' => $this->input->post('last_name',TRUE),
		'username' => $this->input->post('username',TRUE),
		'password' => $this->input->post('password',TRUE),
		'about_me' => $this->input->post('about_me',TRUE),
		'relationship_status' => $this->input->post('relationship_status',TRUE),
		'city' => $this->input->post('city',TRUE),
		'profession' => $this->input->post('profession',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'device_token' => $this->input->post('device_token',TRUE),
		'device_type' => $this->input->post('device_type',TRUE),
	    );

            $this->Users_model->update($this->input->post('user_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('users'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Users_model->get_by_id($id);

        if ($row) {
            $this->Users_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('users'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users'));
        }
    }

    public function _rules() { 
	$this->form_validation->set_rules('first_name', 'first name', 'trim|required');
	$this->form_validation->set_rules('last_name', 'last name', 'trim|required');
	$this->form_validation->set_rules('username', 'username', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');
	$this->form_validation->set_rules('about_me', 'about me', 'trim|required');
	$this->form_validation->set_rules('relationship_status', 'relationship status', 'trim|required');
	$this->form_validation->set_rules('city', 'city', 'trim|required');
	$this->form_validation->set_rules('profession', 'profession', 'trim|required');
	$this->form_validation->set_rules('created_at', 'created at', 'trim|required');
	$this->form_validation->set_rules('device_token', 'device token', 'trim|required');
	$this->form_validation->set_rules('device_type', 'device type', 'trim|required');

	$this->form_validation->set_rules('user_id', 'user_id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "users.xls";
        $judul = "users";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "First Name");
	xlsWriteLabel($tablehead, $kolomhead++, "Last Name");
	xlsWriteLabel($tablehead, $kolomhead++, "Username");
	xlsWriteLabel($tablehead, $kolomhead++, "Password");
	xlsWriteLabel($tablehead, $kolomhead++, "About Me");
	xlsWriteLabel($tablehead, $kolomhead++, "Relationship Status");
	xlsWriteLabel($tablehead, $kolomhead++, "City");
	xlsWriteLabel($tablehead, $kolomhead++, "Profession");
	xlsWriteLabel($tablehead, $kolomhead++, "Created At");
	xlsWriteLabel($tablehead, $kolomhead++, "Device Token");
	xlsWriteLabel($tablehead, $kolomhead++, "Device Type");

	foreach ($this->Users_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->first_name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->last_name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->username);
	    xlsWriteLabel($tablebody, $kolombody++, $data->password);
	    xlsWriteLabel($tablebody, $kolombody++, $data->about_me);
	    xlsWriteLabel($tablebody, $kolombody++, $data->relationship_status);
	    xlsWriteLabel($tablebody, $kolombody++, $data->city);
	    xlsWriteLabel($tablebody, $kolombody++, $data->profession);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_at);
	    xlsWriteLabel($tablebody, $kolombody++, $data->device_token);
	    xlsWriteLabel($tablebody, $kolombody++, $data->device_type);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=users.doc");

        $data = array(
            'users_data' => $this->Users_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('users/users_doc',$data);
    }

}

/* End of file Users.php */
/* Location: ./application/controllers/Users.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-10-05 10:02:20 */
/* http://harviacode.com */
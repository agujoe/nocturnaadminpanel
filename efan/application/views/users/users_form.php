<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Users <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">First Name <?php echo form_error('first_name') ?></label>
            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" value="<?php echo $first_name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Last Name <?php echo form_error('last_name') ?></label>
            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" value="<?php echo $last_name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Username <?php echo form_error('username') ?></label>
            <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Password <?php echo form_error('password') ?></label>
            <input type="text" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
        </div>
	    <div class="form-group">
            <label for="about_me">About Me <?php echo form_error('about_me') ?></label>
            <textarea class="form-control" rows="3" name="about_me" id="about_me" placeholder="About Me"><?php echo $about_me; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="enum">Relationship Status <?php echo form_error('relationship_status') ?></label>
            <input type="text" class="form-control" name="relationship_status" id="relationship_status" placeholder="Relationship Status" value="<?php echo $relationship_status; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">City <?php echo form_error('city') ?></label>
            <input type="text" class="form-control" name="city" id="city" placeholder="City" value="<?php echo $city; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Profession <?php echo form_error('profession') ?></label>
            <input type="text" class="form-control" name="profession" id="profession" placeholder="Profession" value="<?php echo $profession; ?>" />
        </div>
	    <div class="form-group">
            <label for="timestamp">Created At <?php echo form_error('created_at') ?></label>
            <input type="text" class="form-control" name="created_at" id="created_at" placeholder="Created At" value="<?php echo $created_at; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Device Token <?php echo form_error('device_token') ?></label>
            <input type="text" class="form-control" name="device_token" id="device_token" placeholder="Device Token" value="<?php echo $device_token; ?>" />
        </div>
	    <div class="form-group">
            <label for="enum">Device Type <?php echo form_error('device_type') ?></label>
            <input type="text" class="form-control" name="device_type" id="device_type" placeholder="Device Type" value="<?php echo $device_type; ?>" />
        </div>
	    <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('users') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>
<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Users List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Username</th>
		<th>Password</th>
		<th>About Me</th>
		<th>Relationship Status</th>
		<th>City</th>
		<th>Profession</th>
		<th>Created At</th>
		<th>Device Token</th>
		<th>Device Type</th>
		
            </tr><?php
            foreach ($users_data as $users)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $users->first_name ?></td>
		      <td><?php echo $users->last_name ?></td>
		      <td><?php echo $users->username ?></td>
		      <td><?php echo $users->password ?></td>
		      <td><?php echo $users->about_me ?></td>
		      <td><?php echo $users->relationship_status ?></td>
		      <td><?php echo $users->city ?></td>
		      <td><?php echo $users->profession ?></td>
		      <td><?php echo $users->created_at ?></td>
		      <td><?php echo $users->device_token ?></td>
		      <td><?php echo $users->device_type ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>
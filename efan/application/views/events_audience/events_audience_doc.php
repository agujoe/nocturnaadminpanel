<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Events_audience List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Event Id</th>
		<th>User Id</th>
		<th>Invite Id</th>
		<th>Status</th>
		<th>Created At</th>
		
            </tr><?php
            foreach ($events_audience_data as $events_audience)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $events_audience->event_id ?></td>
		      <td><?php echo $events_audience->user_id ?></td>
		      <td><?php echo $events_audience->invite_id ?></td>
		      <td><?php echo $events_audience->status ?></td>
		      <td><?php echo $events_audience->created_at ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>
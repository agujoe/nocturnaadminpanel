<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Posts <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">User Id <?php echo form_error('user_id') ?></label>
            <input type="text" class="form-control" name="user_id" id="user_id" placeholder="User Id" value="<?php echo $user_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="enum">Post Type <?php echo form_error('post_type') ?></label>
            <input type="text" class="form-control" name="post_type" id="post_type" placeholder="Post Type" value="<?php echo $post_type; ?>" />
        </div>
	    <div class="form-group">
            <label for="enum">Status Type <?php echo form_error('status_type') ?></label>
            <input type="text" class="form-control" name="status_type" id="status_type" placeholder="Status Type" value="<?php echo $status_type; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Message <?php echo form_error('message') ?></label>
            <input type="text" class="form-control" name="message" id="message" placeholder="Message" value="<?php echo $message; ?>" />
        </div>
	    <div class="form-group">
            <label for="timestamp">Created At <?php echo form_error('created_at') ?></label>
            <input type="text" class="form-control" name="created_at" id="created_at" placeholder="Created At" value="<?php echo $created_at; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Link <?php echo form_error('link') ?></label>
            <input type="text" class="form-control" name="link" id="link" placeholder="Link" value="<?php echo $link; ?>" />
        </div>
	    <input type="hidden" name="post_id" value="<?php echo $post_id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('posts') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>
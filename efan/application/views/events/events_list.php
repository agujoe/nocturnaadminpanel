<?php
include "header.php";
?>

<?php
include "leftsidebar.php";
?>

<?php
include "topnav.php";
?>

    <div class="right_col" role="main">
        <h2 style="margin-top:0px">Events List</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('events/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('events/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('events'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table id="datatable-buttons" class="table table-striped table-bordered jambo_table bulk_action">
		<thead>
            <tr class="headings">
                <th style="width:3%">No</th>
		<th>User Id</th>
		<th>Title</th>
		<th>Description</th>
		<th>Start Time</th>
		<th>End Time</th>
		<th>Start Date</th>
		<th>End Date</th>
		<th>Can Invite</th>
		<th>Location</th>
		<th>Event Pic</th>
		<th>Visitors</th>
		<th>Event Type</th>
		<th>Created At</th>
		 <th style="width:13%">Action</th>
            </tr>
			</thead>
			<?php
            foreach ($events_data as $events)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $events->user_id ?></td>
			<td><?php echo $events->title ?></td>
			<td><?php echo $events->description ?></td>
			<td><?php echo $events->start_time ?></td>
			<td><?php echo $events->end_time ?></td>
			<td><?php echo $events->start_date ?></td>
			<td><?php echo $events->end_date ?></td>
			<td><?php echo $events->can_invite ?></td>
			<td><?php echo $events->location ?></td>
			<td><?php echo $events->event_pic ?></td>
			<td><?php echo $events->visitors ?></td>
			<td><?php echo $events->event_type ?></td>
			<td><?php echo $events->created_at ?></td>
			<td>
			<div class="col-md-12">
            <div class="btn-group">
                              
           <a href="<?php echo base_url('events/read/'.$events->event_id);?>"><button class="btn btn-sm btn-success" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Read"><i class="fa fa-book"></i></button></a> 
          <a href="<?php echo base_url('events/update/'.$events->event_id);?>"><button class="btn btn-sm btn-primary" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Update"><i class="fa fa-edit"></i></button></a> 
			<a href="<?php echo base_url('events/delete/'.$events->event_id);?>"><button class="btn btn-sm btn-danger" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></button></a> 
			 </div>
                            </div>
			</td>
			
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-danger">Total Record : <?php echo $total_rows ?></a>
		<?php echo anchor(site_url('events/excel'), 'Excel', 'class="btn btn-success"'); ?>
		<?php echo anchor(site_url('events/word'), 'Word', 'class="btn btn-primary"'); ?>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
  </div>
   <?php
include "footer.php";
?>
<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/dist/css/bootstrap.min.css') ?>"/>
        
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Events List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>User Id</th>
		<th>Title</th>
		<th>Description</th>
		<th>Start Time</th>
		<th>End Time</th>
		<th>Start Date</th>
		<th>End Date</th>
		<th>Can Invite</th>
		<th>Location</th>
		<th>Event Pic</th>
		<th>Visitors</th>
		<th>Event Type</th>
		<th>Created At</th>
		
            </tr><?php
            foreach ($events_data as $events)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $events->user_id ?></td>
		      <td><?php echo $events->title ?></td>
		      <td><?php echo $events->description ?></td>
		      <td><?php echo $events->start_time ?></td>
		      <td><?php echo $events->end_time ?></td>
		      <td><?php echo $events->start_date ?></td>
		      <td><?php echo $events->end_date ?></td>
		      <td><?php echo $events->can_invite ?></td>
		      <td><?php echo $events->location ?></td>
		      <td><?php echo $events->event_pic ?></td>
		      <td><?php echo $events->visitors ?></td>
		      <td><?php echo $events->event_type ?></td>
		      <td><?php echo $events->created_at ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>
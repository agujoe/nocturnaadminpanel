<?php
include "header.php";
?>

<?php
include "leftsidebar.php";
?>

<?php
include "topnav.php";
?>

    <div class="right_col" role="main">
        <h2 style="margin-top:0px">Events Read</h2>
        <table class="table">
	    <tr><td>User Id</td><td><?php echo $user_id; ?></td></tr>
	    <tr><td>Title</td><td><?php echo $title; ?></td></tr>
	    <tr><td>Description</td><td><?php echo $description; ?></td></tr>
	    <tr><td>Start Time</td><td><?php echo $start_time; ?></td></tr>
	    <tr><td>End Time</td><td><?php echo $end_time; ?></td></tr>
	    <tr><td>Start Date</td><td><?php echo $start_date; ?></td></tr>
	    <tr><td>End Date</td><td><?php echo $end_date; ?></td></tr>
	    <tr><td>Can Invite</td><td><?php echo $can_invite; ?></td></tr>
	    <tr><td>Location</td><td><?php echo $location; ?></td></tr>
	    <tr><td>Event Pic</td><td><?php echo $event_pic; ?></td></tr>
	    <tr><td>Visitors</td><td><?php echo $visitors; ?></td></tr>
	    <tr><td>Event Type</td><td><?php echo $event_type; ?></td></tr>
	    <tr><td>Created At</td><td><?php echo $created_at; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('events') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
     </div>
	 
    <?php
include "footer.php";
?>
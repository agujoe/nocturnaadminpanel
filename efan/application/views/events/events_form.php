<?php
include "header.php";
?>

<?php
include "leftsidebar.php";
?>

<?php
include "topnav.php";
?>

    <div class="right_col" role="main">
      <h2 style="margin-top:0px">Events <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">User Id <?php echo form_error('user_id') ?></label>
            <input type="text" class="form-control" name="user_id" id="user_id" placeholder="User Id" value="<?php echo $user_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Title <?php echo form_error('title') ?></label>
            <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?php echo $title; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Description <?php echo form_error('description') ?></label>
            <input type="text" class="form-control" name="description" id="description" placeholder="Description" value="<?php echo $description; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Start Time <?php echo form_error('start_time') ?></label>
            <input type="text" class="form-control" name="start_time" id="start_time" placeholder="Start Time" value="<?php echo $start_time; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">End Time <?php echo form_error('end_time') ?></label>
            <input type="text" class="form-control" name="end_time" id="end_time" placeholder="End Time" value="<?php echo $end_time; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Start Date <?php echo form_error('start_date') ?></label>
            <input type="text" class="form-control" name="start_date" id="start_date" placeholder="Start Date" value="<?php echo $start_date; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">End Date <?php echo form_error('end_date') ?></label>
            <input type="text" class="form-control" name="end_date" id="end_date" placeholder="End Date" value="<?php echo $end_date; ?>" />
        </div>
	    <div class="form-group">
            <label for="enum">Can Invite <?php echo form_error('can_invite') ?></label>
            <input type="text" class="form-control" name="can_invite" id="can_invite" placeholder="Can Invite" value="<?php echo $can_invite; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Location <?php echo form_error('location') ?></label>
            <input type="text" class="form-control" name="location" id="location" placeholder="Location" value="<?php echo $location; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Event Pic <?php echo form_error('event_pic') ?></label>
            <input type="text" class="form-control" name="event_pic" id="event_pic" placeholder="Event Pic" value="<?php echo $event_pic; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Visitors <?php echo form_error('visitors') ?></label>
            <input type="text" class="form-control" name="visitors" id="visitors" placeholder="Visitors" value="<?php echo $visitors; ?>" />
        </div>
	    <div class="form-group">
            <label for="enum">Event Type <?php echo form_error('event_type') ?></label>
            <input type="text" class="form-control" name="event_type" id="event_type" placeholder="Event Type" value="<?php echo $event_type; ?>" />
        </div>
	    <div class="form-group">
            <label for="timestamp">Created At <?php echo form_error('created_at') ?></label>
            <input type="text" class="form-control" name="created_at" id="created_at" placeholder="Created At" value="<?php echo $created_at; ?>" />
        </div>
	    <input type="hidden" name="event_id" value="<?php echo $event_id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('events') ?>" class="btn btn-default">Cancel</a>
	</form>
	</div>
    <?php
include "footer.php";
?>

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class NormalUser extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('NormalUser_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'normaluser?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'normaluser?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'normaluser/';
            $config['first_url'] = base_url() . 'normaluser/';
        }

        $config['per_page'] = 12;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->NormalUser_model->total_rows($q);
        $normaluser = $this->NormalUser_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'normaluser_data' => $normaluser,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'image_url'=>ORIGINAL_URL,
        );
		$this->load->view('layout/index', $data);
        $this->load->view('normaluser/normaluser_list', $data);
    }

    public function read()
    {
        $id = $this->uri->segment(3);
        $row = $this->NormalUser_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'googlePlus' => $row->googlePlus,
		        'faceBook' => $row->faceBook,
		        'userName' => $row->userName,
		        'fullName' => $row->fullName,
		        'age' => $row->age,
		        'gender' => $row->gender,
		        'city' => $row->city,
		        'email' => $row->email,
		        'password' => $row->password,
                'phoneNumber' => $row->phoneNumber,
                'intrest' => $row->intrest,
                'dis' => $row->dis,
                'deviceToken' => $row->deviceToken,
                'typeDevice' => $row->typeDevice,
                'profilePicture' => $row->profilePicture,
            );

            $this->load->view('layout/index', $data);
            $this->load->view('normaluser/normaluser_read', $data);

        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('NormalUser'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'image_url' => ORIGINAL_URL,
            'action' => site_url('normaluser/create_action'),
            'id' => set_value('id'),
            'userName' => set_value('userName'),
	        'fullName' => set_value('fullName'),
	        'age' => set_value('age'),
	        'gender' => set_value('gender'),
	        'city' => set_value('city'),
	        'email' => set_value('email'),
	        'password' => set_value('password'),
            'typeDevice' => set_value('typeDevice'),
	        'phoneNumber' => set_value('phoneNumber'),
	        // 'intrest' => set_value('intrest'),
	        // 'dis' => set_value('dis'),
	        // 'deviceToken' => set_value('deviceToken'),
            // 'googlePlus' => set_value('googlePlus'),
            // 'faceBook' => set_value('faceBook'),
            'profilePicture' => set_value('profilePicture'),
        );

        $this->load->view('layout/index');
        $this->load->view('normaluser/normaluser_form', $data);
    }

    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
            $this->session->set_flashdata('message', 'An error occurred');
        } else {

            //add image
            $imgName = md5(time()). ".png";
            $imgPath = getcwd()."/api/uploads/normal_user/profile/".$imgName;

            if (isset($_FILES["profilePicture"]["tmp_name"]) && move_uploaded_file($_FILES["profilePicture"]["tmp_name"],$imgPath) )
            {
                // echo 'uploaded';die;
            }

            $data = array(
                'userName' => $this->input->post('userName',TRUE),
		        'fullName' => $this->input->post('fullName',TRUE),
		        'email' => $this->input->post('email',TRUE),
                'password' => $this->input->post('password',TRUE),
                'phoneNumber' => $this->input->post('phoneNumber',TRUE),
                'typeDevice' => $this->input->post('typeDevice',TRUE),
		        // 'intrest' => $this->input->post('intrest',TRUE),
		        // 'dis' => $this->input->post('dis',TRUE),
		        // 'deviceToken' => $this->input->post('deviceToken',TRUE),
                // 'googlePlus' => $this->input->post('googlePlus',TRUE),
                // 'faceBook' => $this->input->post('faceBook',TRUE),
                // 'age' => $this->input->post('age',TRUE),
                //'gender' => $this->input->post('gender',TRUE),
                //'city' => $this->input->post('city',TRUE),
                'profilePicture' => $imgName,
            );

            $this->NormalUser_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');

            redirect(site_url('NormalUser'));
        }
    }
    
    public function update()
    {
        $id = $this->uri->segment(3);
        $row = $this->NormalUser_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'image_url'=>ORIGINAL_URL,
                'action' => site_url('normaluser/update_action'),
		        'id' => set_value('id', $row->id),
		        'userName' => set_value('userName', $row->userName),
		        'fullName' => set_value('fullName', $row->fullName),
		        'age' => set_value('age', $row->age),
		        'gender' => set_value('gender', $row->gender),
		        'city' => set_value('city', $row->city),
		        'email' => set_value('email', $row->email),
		        'password' => set_value('password', $row->password),
		        'phoneNumber' => set_value('phoneNumber', $row->phoneNumber),
		        // 'intrest' => set_value('intrest', $row->intrest),
		        // 'dis' => set_value('dis', $row->dis),
		        // 'deviceToken' => set_value('deviceToken', $row->deviceToken),
		        // 'typeDevice' => set_value('typeDevice', $row->typeDevice),
                // 'googlePlus' => set_value('googlePlus', $row->googlePlus),
                // 'faceBook' => set_value('faceBook', $row->faceBook),
                'profilePicture' => set_value('profilePicture', $row->profilePicture),
            );

            $this->load->view('layout/index', $data);
            $this->load->view('normaluser/normaluser_form', $data);

        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('NormalUser'));
        }
    }
    
    public function update_action() 
    {
        $id = $this->input->post('id', true);
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {

            //add image
            $imgName = md5(time()). ".png";
            $imgPath = getcwd()."/api/uploads/normal_user/profile/".$imgName;
            
            $data = array(
		        'userName' => $this->input->post('userName',TRUE),
		        'fullName' => $this->input->post('fullName',TRUE),
		        'age' => $this->input->post('age',TRUE),
		        'gender' => $this->input->post('gender',TRUE),
		        'city' => $this->input->post('city',TRUE),
		        'email' => $this->input->post('email',TRUE),
		        'password' => $this->input->post('password',TRUE),
		        'phoneNumber' => $this->input->post('phoneNumber',TRUE),
                'typeDevice' => $this->input->post('typeDevice',TRUE),
                'profilePicture' => $this->input->post('profilePicture',TRUE),
                // 'intrest' => $this->input->post('intrest',TRUE),
	        	// 'dis' => $this->input->post('dis',TRUE),
		        // 'deviceToken' => $this->input->post('deviceToken',TRUE),
		        // 'profilePicture' => "upload/".$imgName,
                // 'googlePlus' => $this->input->post('googlePlus',TRUE),
                // 'faceBook' => $this->input->post('faceBook',TRUE),
            );

            if (isset($_FILES["profilePicture"]["tmp_name"]) && move_uploaded_file($_FILES["profilePicture"]["tmp_name"],$imgPath))
            {
                   $data['profilePicture'] = $imgName;
            }

            $this->NormalUser_model->update($id, $data);
            $this->session->set_flashdata('message', 'Record Updated Successfully');
            redirect(site_url('NormalUser'));
        }
    }
    
    public function delete()
    {
        $id = $this->uri->segment(3);
        $row = $this->NormalUser_model->get_by_id($id);
        if ($row) {
            $this->NormalUser_model->delete($id);
            $this->session->set_flashdata('message', 'Record Delete Successfully');
            redirect(site_url('NormalUser'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('NormalUser'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('userName', 'username', 'trim|required');
        $this->form_validation->set_rules('fullName', 'fullname', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('phoneNumber', 'phonenumber', 'trim|required');
        $this->form_validation->set_rules('typeDevice', 'typedevice', 'trim|required');
        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
        //$this->form_validation->set_rules('googlePlus', 'googleplus', 'trim|required');
        //$this->form_validation->set_rules('faceBook', 'facebook', 'trim|required');
        // $this->form_validation->set_rules('age', 'age', 'trim|required');
        // $this->form_validation->set_rules('gender', 'gender', 'trim|required');
        // $this->form_validation->set_rules('city', 'city', 'trim|required');
        // $this->form_validation->set_rules('intrest', 'intrest', 'trim|required');
        // $this->form_validation->set_rules('dis', 'dis', 'trim|required');
        // $this->form_validation->set_rules('deviceToken', 'devicetoken', 'trim|required');
        // $this->form_validation->set_rules('profilePicture', 'profilepicture', 'trim|required');

    }
}
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notification extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Notification_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'notification?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'notification?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'notification/';
            $config['first_url'] = base_url() . 'notification/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Notification_model->total_rows($q);
        $notification = $this->Notification_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'notification_data' => $notification,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
		$this->load->view('header.php', $data);
		$this->load->view('leftsidebar.php', $data);
		$this->load->view('topnav.php', $data);
        $this->load->view('notification/notification_list', $data);
		$this->load->view('footer.php', $data);
    }

    public function read($id) 
    {
        $row = $this->Notification_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'userID' => $row->userID,
		// 'friendID' => $row->friendID,
		'text' => $row->text,
		// 'notificationType' => $row->notificationType,
		// 'isRead' => $row->isRead,
		'date' => $row->date,
	    );
			$this->load->view('header.php', $data);
			$this->load->view('leftsidebar.php', $data);
			$this->load->view('topnav.php', $data);
            $this->load->view('notification/notification_read', $data);
			$this->load->view('footer.php', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('notification'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('notification/create_action'),
	    'id' => set_value('id'),
	    'userID' => set_value('userID'),
	    // 'friendID' => set_value('friendID'),
	    'text' => set_value('text'),
	    // 'notificationType' => set_value('notificationType'),
	    // 'isRead' => set_value('isRead'),
	    'date' => set_value('date'),
	);
			$this->load->view('header.php', $data);
			$this->load->view('leftsidebar.php', $data);
			$this->load->view('topnav.php', $data);
			$this->load->view('notification/notification_form', $data);
			$this->load->view('footer.php', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'userID' => $this->input->post('userID',TRUE),
		// 'friendID' => $this->input->post('friendID',TRUE),
		'text' => $this->input->post('text',TRUE),
		// 'notificationType' => $this->input->post('notificationType',TRUE),
		// 'isRead' => $this->input->post('isRead',TRUE),
		'date' => $this->input->post('date',TRUE),
	    );

            $this->Notification_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('notification'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Notification_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('notification/update_action'),
		'id' => set_value('id', $row->id),
		'userID' => set_value('userID', $row->userID),
		// 'friendID' => set_value('friendID', $row->friendID),
		'text' => set_value('text', $row->text),
		// 'notificationType' => set_value('notificationType', $row->notificationType),
		// 'isRead' => set_value('isRead', $row->isRead),
		'date' => set_value('date', $row->date),
	    );
			$this->load->view('header.php', $data);
			$this->load->view('leftsidebar.php', $data);
			$this->load->view('topnav.php', $data);
            $this->load->view('notification/notification_form', $data);
			$this->load->view('footer.php', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('notification'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'userID' => $this->input->post('userID',TRUE),
		// 'friendID' => $this->input->post('friendID',TRUE),
		'text' => $this->input->post('text',TRUE),
		// 'notificationType' => $this->input->post('notificationType',TRUE),
		// 'isRead' => $this->input->post('isRead',TRUE),
		'date' => $this->input->post('date',TRUE),
	    );

            $this->Notification_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('notification'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Notification_model->get_by_id($id);

        if ($row) {
            $this->Notification_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('notification'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('notification'));
        }
    }

    public function _rules() { 
	$this->form_validation->set_rules('userID', 'userid', 'trim|required');
	// $this->form_validation->set_rules('friendID', 'friendid', 'trim|required');
	$this->form_validation->set_rules('text', 'text', 'trim|required');
	// $this->form_validation->set_rules('notificationType', 'notificationtype', 'trim|required');
	// $this->form_validation->set_rules('isRead', 'isread', 'trim|required');
	$this->form_validation->set_rules('date', 'date', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Notification.php */
/* Location: ./application/controllers/Notification.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-10-05 09:59:57 */
/* http://harviacode.com */
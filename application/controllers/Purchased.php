<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Purchased extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Purchased_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'purchased?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'purchased?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'purchased/';
            $config['first_url'] = base_url() . 'purchased/';
        }

        $config['per_page'] = 12;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Purchased_model->total_rows($q);
        $purchased = $this->Purchased_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'purchased_data' => $purchased,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

        $this->load->view('layout/index', $data);
        $this->load->view('purchased/purchased_list', $data);

    }

    public function read($id) 
    {
        $row = $this->Purchased_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'ownerID' => $row->ownerID,
                'nameOfClub' => $row->nameOfClub,
                'price' => $row->price,
                'date' => $row->date,
            );

            $this->load->view('layout/index', $data);
            $this->load->view('purchased/purchased_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('purchased'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('purchased/create_action'),
            'id' => set_value('id'),
            'ownerID' => set_value('ownerID'),
            'nameOfClub' => set_value('nameOfClub'),
            'price' => set_value('price'),
            'date' => set_value('date'),
            );

        $this->load->view('layout/index', $data);
        $this->load->view('purchased/purchased_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'ownerID' => $this->input->post('ownerID',TRUE),
		'nameOfClub' => $this->input->post('nameOfClub',TRUE),
		'price' => $this->input->post('price',TRUE),
		'date' => $this->input->post('date',TRUE),
	    );

            $this->Purchased_model->insert($data);
            $this->session->set_flashdata('message', 'Created Record Successfully');
            redirect(site_url('purchased'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Purchased_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('purchased/update_action'),
                'id' => set_value('id', $row->id),
                'ownerID' => set_value('ownerID', $row->ownerID),
                'nameOfClub' => set_value('nameOfClub', $row->nameOfClub),
                'price' => set_value('price', $row->price),
                'date' => set_value('date', $row->date),
            );

            $this->load->view('layout/index', $data);
            $this->load->view('purchased/purchased_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('purchased'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'ownerID' => $this->input->post('ownerID',TRUE),
                'nameOfClub' => $this->input->post('nameOfClub',TRUE),
                'price' => $this->input->post('price',TRUE),
                'date' => $this->input->post('date',TRUE),
            );

            $this->Purchased_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Updated Record Successfully');
            redirect(site_url('purchased'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Purchased_model->get_by_id($id);

        if ($row) {
            $this->Purchased_model->delete($id);
            $this->session->set_flashdata('message', 'Record Deleted Successfully');
            redirect(site_url('purchased'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('purchased'));
        }
    }

    public function _rules() {

        $this->form_validation->set_rules('ownerID', 'ownerID', 'trim|required');
        $this->form_validation->set_rules('nameOfClub', 'nameOfClub', 'trim|required');
        $this->form_validation->set_rules('price', 'price', 'trim|required');
        $this->form_validation->set_rules('date', 'date', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

    }

}
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Post extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Post_model');
        $this->load->library('form_validation');
        // $this->load->library('upload', $configVideo);
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'post?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'post?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'post/';
            $config['first_url'] = base_url() . 'post/';
        }

        $config['per_page'] = 12;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Post_model->total_rows($q);
        $post = $this->Post_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'post_data' => $post,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'image_url' => ORIGINAL_URL3,
            'video_url' => ORIGINAL_URL4,
            'start' => $start,
        );

        $this->load->view('layout/index', $data);
        $this->load->view('post/post_list', $data);

    }

    public function read($id) 
    {
        $row = $this->Post_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'userID' => $row->userID,
                'picture' => $row->picture,
                'status' => $row->status,
                'checkIn' => $row->checkIn,
                'video' => $row->video,
                'statusType' => $row->statusType,
                'date' => $row->date,
            );

            $this->load->view('layout/index', $data);
            $this->load->view('post/post_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('post'));
        }
    }

    public function create() 
    {
        $data = array(
            'image_url'=>ORIGINAL_URL3,
            'video_url'=>ORIGINAL_URL4,
            'button' => 'Create',
            'action' => site_url('post/create_action'),
            'id' => set_value('id'),
            'userID' => set_value('userID'),
            'picture' => set_value('picture'),
            'status' => set_value('status'),
            'checkIn' => set_value('checkIn'),
            'video' => set_value('video'),
            'statusType' => set_value('statusType'),
            'date' => set_value('date'),
        );

        $this->load->view('layout/index', $data);
        $this->load->view('post/post_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            //add image
            $imgName = md5(time()). ".png";
            $vidName = md5(time()). ".mp4";
            $imgPath = getcwd()."/api/uploads/pictureStatus/".$imgName;
            $vidPath = getcwd()."/api/uploads/videoStatus/".$vidName;

            if (isset($_FILES["picture"]["tmp_name"]) && move_uploaded_file($_FILES["picture"]["tmp_name"],$imgPath) )
            {
                // echo 'uploaded';die;
            }

            if (isset($_FILES["video"]["tmp_name"]) && move_uploaded_file($_FILES["video"]["tmp_name"],$vidPath) )
            {
                // echo 'uploaded';die;
            } 


            $data = array(
                'userID' => $this->input->post('userID',TRUE),
                'picture' => $imgName,
                'status' => $this->input->post('status',TRUE),
                'checkIn' => $this->input->post('checkIn',TRUE),
                'video' => $vidName,
                'statusType' => $this->input->post('statusType',TRUE),
                'date' => $this->input->post('date',TRUE),
            );

            $this->Post_model->insert($data);
            $this->session->set_flashdata('message', 'Record Created Successfully');
            redirect(site_url('post'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Post_model->get_by_id($id);

        if ($row) {
            $data = array(
                'image_url'=>ORIGINAL_URL3,
                'video_url'=>ORIGINAL_URL4,
                'button' => 'Update',
                'action' => site_url('post/update_action'),
                'id' => set_value('id', $row->id),
                'userID' => set_value('userID', $row->userID),
                'picture' => set_value('picture', $row->picture),
                'status' => set_value('status', $row->status),
                'checkIn' => set_value('checkIn', $row->checkIn),
                'video' => set_value('video', $row->video),
                'statusType' => set_value('statusType', $row->statusType),
                'date' => set_value('date', $row->date),
            );

            $this->load->view('layout/index', $data);
            $this->load->view('post/post_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('post'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {

             $filepath= $_SERVER['DOCUMENT_ROOT']. '/api/';
           
            $imgName = md5(time()). ".png";
            $vidName = md5(time()). ".mp4";
            $imgPath = getcwd()."/api/uploads/pictureStatus/".$imgName;
            $vidPath = getcwd()."/api/uploads/videoStatus/".$vidName;

            $data = array(
                'userID' => $this->input->post('userID',TRUE),
                // 'picture' => "upload/".$imgName,
                'status' => $this->input->post('status',TRUE),
                'checkIn' => $this->input->post('checkIn',TRUE),
                'video' => $this->input->post('video',TRUE),
                // 'video' => "upload/".$vidName,
                'statusType' => $this->input->post('statusType',TRUE),
                'date' => $this->input->post('date',TRUE),
            );

            if (isset($_FILES["picture"]["tmp_name"]) && move_uploaded_file($_FILES["picture"]["tmp_name"],$imgPath) )
            {
                $data['picture'] = $imgName;
            }

            // echo print_r($_FILES);die;
            if (isset($_FILES["video"]["tmp_name"]) && move_uploaded_file($_FILES["video"]["tmp_name"],$vidPath))
            {
                // print_r($_FILES["video"]["tmp_name"]);
                // die;
                // echo "uploaded";die;
                $data['video'] = $vidName;
            } 
            // else{
            //     echo "not uploaded";die;
            // }


            $this->Post_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('post'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Post_model->get_by_id($id);

        if ($row) {
            $this->Post_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('post'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('post'));
        }
    }

    public function _rules() {

        $this->form_validation->set_rules('userID', 'userid', 'trim|required');
        // $this->form_validation->set_rules('picture', 'picture', 'trim|required');
        $this->form_validation->set_rules('status', 'status', 'trim|required');
        $this->form_validation->set_rules('checkIn', 'checkin', 'trim|required');
        // $this->form_validation->set_rules('video', 'video', 'trim|required');
        $this->form_validation->set_rules('statusType', 'statustype', 'trim|required');
        $this->form_validation->set_rules('date', 'date', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

    }

}
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Owneruser extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Owneruser_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'owneruser?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'owneruser?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'owneruser/';
            $config['first_url'] = base_url() . 'owneruser/';
        }

        $config['per_page'] = 12;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Owneruser_model->total_rows($q);
        $owneruser = $this->Owneruser_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'owneruser_data' => $owneruser,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'image_url'=>ORIGINAL_URL1,
        );

        $this->load->view('layout/index', $data);
        $this->load->view('owneruser/owneruser_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Owneruser_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'userName' => $row->userName,
                'nameOfClub' => $row->nameOfClub,
                'startTime' => $row->startTime,
                'endTime' => $row->endTime,
                'clubDis' => $row->clubDis,
                'password' => $row->password,
                'phoneNumber' => $row->phoneNumber,
                'address' => $row->address,
                'email' => $row->email,
                'deviceToken' => $row->deviceToken,
                'clubType' => $row->clubType,
                'typeDevice' => $row->typeDevice,
                'profile' => $row->profile,
                'cover' => $row->cover,
                'lat' => $row->lat,
                'long' => $row->long,
            );

            $this->load->view('layout/index', $data);
            $this->load->view('owneruser/owneruser_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('owneruser'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'image_url'=>ORIGINAL_URL1,
            'action' => site_url('owneruser/create_action'),
            'id' => set_value('id'),
            'userName' => set_value('userName'),
            'email' => set_value('email'),
            'profile' => set_value('profile'),
            // 'nameOfClub' => set_value('nameOfClub'),
            // 'startTime' => set_value('startTime'),
            // 'endTime' => set_value('endTime'), // 'password' => set_value('password'),
            // 'phoneNumber' => set_value('phoneNumber'),
            // 'address' => set_value('address'),
            // 'clubDis' => set_value('clubDis'),
            // 'deviceToken' => set_value('deviceToken'),
            // 'clubType' => set_value('clubType'),
            // 'typeDevice' => set_value('typeDevice'),
            // 'cover' => set_value('cover'),
            // 'lat' => set_value('lat'),
            // 'long' => set_value('long'),
        );

        $this->load->view('layout/index', $data);
        $this->load->view('owneruser/owneruser_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            //add image
            $imgName = md5(time()). ".png";
            $imgPath = getcwd()."/api/uploads/owner_user/profile/".$imgName;

            if (isset($_FILES["profile"]["tmp_name"]) && move_uploaded_file($_FILES["profile"]["tmp_name"],$imgPath) )
            {
                // echo 'uploaded';die;
            }

            $data = array(
                'userName' => $this->input->post('userName',TRUE),
                // 'nameOfClub' => $this->input->post('nameOfClub',TRUE),
                // 'startTime' => $this->input->post('startTime',TRUE),
                // 'endTime' => $this->input->post('endTime',TRUE),
                // 'clubDis' => $this->input->post('clubDis',TRUE),
                // 'password' => $this->input->post('password',TRUE),
                'password' => uniqid(),
                // 'phoneNumber' => $this->input->post('phoneNumber',TRUE),
                // 'address' => $this->input->post('address',TRUE),
                'email' => $this->input->post('email',TRUE),
                // 'deviceToken' => $this->input->post('deviceToken',TRUE),
                // 'clubType' => $this->input->post('clubType',TRUE),
                // 'typeDevice' => $this->input->post('typeDevice',TRUE),
                'profile' => $imgName,
                // 'cover' => $this->input->post('cover',TRUE),
                // 'lat' => $this->input->post('lat',TRUE),
                // 'long' => $this->input->post('long',TRUE),
            );

            $this->Owneruser_model->insert($data);
            $this->session->set_flashdata('message', 'Created Record Successfully');
            redirect(site_url('owneruser'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Owneruser_model->get_by_id($id);

        if ($row) {
            $data = array(
                'image_url'=>ORIGINAL_URL1,
                'button' => 'Update',
                'action' => site_url('owneruser/update_action'),
                'id' => set_value('id', $row->id),
                'userName' => set_value('userName', $row->userName),
                'email' => set_value('email', $row->email),
                'profile' => set_value('profile', $row->profile),
                // 'nameOfClub' => set_value('nameOfClub', $row->nameOfClub),
                // 'startTime' => set_value('startTime', $row->startTime),
                // 'endTime' => set_value('endTime', $row->endTime),
                // 'clubDis' => set_value('clubDis', $row->clubDis),
                // 'password' => set_value('password', $row->password),
                // 'phoneNumber' => set_value('phoneNumber', $row->phoneNumber),
                // 'address' => set_value('address', $row->address),
                // 'deviceToken' => set_value('deviceToken', $row->deviceToken),
                // 'clubType' => set_value('clubType', $row->clubType),
                // 'typeDevice' => set_value('typeDevice', $row->typeDevice),
                // 'cover' => set_value('cover', $row->cover),
                // 'lat' => set_value('lat', $row->lat),
                // 'long' => set_value('long', $row->long),
            );

            $this->load->view('layout/index', $data);
            $this->load->view('owneruser/owneruser_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('owneruser'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {

            //add image
            $imgName = md5(time()). ".png";
            $imgPath = getcwd()."/api/uploads/owner_user/profile/".$imgName;


            $data = array(
                'userName' => $this->input->post('userName',TRUE),
                // 'nameOfClub' => $this->input->post('nameOfClub',TRUE),
                // 'startTime' => $this->input->post('startTime',TRUE),
                // 'endTime' => $this->input->post('endTime',TRUE),
                // 'clubDis' => $this->input->post('clubDis',TRUE),
                // 'password' => $this->input->post('password',TRUE),
                // 'phoneNumber' => $this->input->post('phoneNumber',TRUE),
                // 'address' => $this->input->post('address',TRUE),
                'email' => $this->input->post('email',TRUE),
                // 'deviceToken' => $this->input->post('deviceToken',TRUE),
                // 'clubType' => $this->input->post('clubType',TRUE),
                // 'typeDevice' => $this->input->post('typeDevice',TRUE),
                // 'profile' => "upload/".$imgName,
                // 'profile' => $this->input->post('profile',TRUE),
                // 'cover' => $this->input->post('cover',TRUE),
                // 'lat' => $this->input->post('lat',TRUE),
                // 'long' => $this->input->post('long',TRUE),
            );

            if (isset($_FILES["profile"]["tmp_name"]) && move_uploaded_file($_FILES["profile"]["tmp_name"],$imgPath) )
            {
                $data['profile'] = $imgName;
            }

            $this->Owneruser_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Updated Record Successfully');
            redirect(site_url('owneruser'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Owneruser_model->get_by_id($id);

        if ($row) {
            $this->Owneruser_model->delete($id);
            $this->session->set_flashdata('message', 'Deleted Record Successfully');
            redirect(site_url('owneruser'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('owneruser'));
        }
    }

    public function _rules()
    {

        $this->form_validation->set_rules('userName', 'username', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
        // $this->form_validation->set_rules('nameOfClub', 'nameofclub', 'trim|required');
        // $this->form_validation->set_rules('startTime', 'starttime', 'trim|required');
        // $this->form_validation->set_rules('endTime', 'endtime', 'trim|required');
        // $this->form_validation->set_rules('clubDis', 'clubdis', 'trim|required');
        // $this->form_validation->set_rules('password', 'password', 'trim|required');
        // $this->form_validation->set_rules('phoneNumber', 'phonenumber', 'trim|required');
        // $this->form_validation->set_rules('address', 'address', 'trim|required');
        // $this->form_validation->set_rules('deviceToken', 'devicetoken', 'trim|required');
        // $this->form_validation->set_rules('clubType', 'clubtype', 'trim|required');
        // $this->form_validation->set_rules('typeDevice', 'typedevice', 'trim|required');
        // $this->form_validation->set_rules('profile', 'profile', 'trim|required');
        // $this->form_validation->set_rules('cover', 'cover', 'trim|required');
        // $this->form_validation->set_rules('lat', 'lat', 'trim|required');
        // $this->form_validation->set_rules('long', 'long', 'trim|required');
    }

}
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_login_model');
        $this->load->library('form_validation');
        $this->load->model('Post_model');
        $this->load->library('session');
        $this->load->helper('form');

    }

    public function template(){
        $this->load->view('layout/index');
    }

    public function login(){
        $base_url = $this->config->item('base_url');
        $data = $this->input->post();
        $username = $data['username'];
        $password = $data['password'];

        //check if user exists
        $check_user = $this->Admin_login_model->login($username, $password);
        if(!is_null($check_user)){
            redirect('admin/home', 'refresh');
        } else {
            $error_message = "Incorrect username or password";
            $this->session->set_flashdata('error', $error_message);
            redirect($base_url);
        }
    }

    public function index() {
        $this->form_validation->set_rules('username','Username');
        $this->form_validation->set_rules('password','Password');

        if($this->form_validation->run() == TRUE) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $checkname = $this->Admin_login_model->checkname($username);
            $checkpassword = $this->Admin_login_model->checkpassword($password);

            if($checkname['name'] > 0){
                $error_message = "Incorrect username or password";
                $this->session->set_flashdata('error', $error_message);
            } elseif($checkpassword['password'] > 0){
                $error_message = "Incorrect username or password";
                $this->session->set_flashdata('error', $error_message);
                $this->load->view('admin_login');
            }
        } else {
            $this->load->view('admin_login');
        }
    }

    public function show_home(){
        $this->load->view('index');
    }

    public function home()
    {
        //get all counts from each required table
        $normalUserCount = $this->Admin_login_model->allNormalUser();
        $placeOwnerCount = $this->Admin_login_model->allPlaceOwner();
        $packageCount = $this->Admin_login_model->allPackage();
        $postCount = $this->Admin_login_model->allPost();
        $cartCount = $this->Admin_login_model->allCartItem();
        $purchaseCount = $this->Admin_login_model->allPurchaseItem();

        //other data needed for display

        $data = [
            "normalUserCount" => $normalUserCount,
            "placeOwnerCount" => $placeOwnerCount,
            "packageCount" => $packageCount,
            "postCount" => $postCount,
            "cartCount" => $cartCount,
            "purchaseCount" => $purchaseCount

        ];

        $this->load->view('home', $data);
    }

    public function postFrequency()
    {
        //echo "it got this";
        $data = $this->Post_model->getPostPerMonth();
        echo json_encode($data);
    }

    /** This endpoint returns data corresponding to which device is most used by customers
     * either android of iOS.
     */
    public function deviceRatio()
    {
        $device_count = $this->Admin_login_model->getDeviceRatio();
        echo json_encode($device_count);
    }

    public function normalUserAgainstPlaceOwner()
    {
        $all_users = $this->Admin_login_model->getNormalUserAgainstPlaceOwners();
        echo json_encode($all_users);
    }

    public function purchaseStatusRatio()
    {
        $all_purchases = $this->Admin_login_model->getPaymentStatusRatio();
        echo json_encode($all_purchases);
    }
}

?>
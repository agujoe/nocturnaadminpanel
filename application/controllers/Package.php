<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Package extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Package_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'package?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'package?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'package/';
            $config['first_url'] = base_url() . 'package/';
        }

        $config['per_page'] = 12;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Package_model->total_rows($q);
        $package = $this->Package_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'package_data' => $package,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'image_url'=>ORIGINAL_URL2,
        );

        $this->load->view('layout/index', $data);
        $this->load->view('package/package_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Package_model->get_by_id($id);
        
        if ($row) {
            $data = array(
                'id' => $row->id,
                'ownerID' => $row->ownerID,
                'nameOfClub' => $row->nameOfClub,
                'price' => $row->price,
                'description_attributes' => $row->description_attributes,
                'validity' => $row->validity,
                'discount' => $row->discount,
                'image' => $row->image,
            );

            $this->load->view('layout/index', $data);
            $this->load->view('package/package_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('package'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'image_url'=>ORIGINAL_URL2,
            'action' => site_url('package/create_action'),
            'id' => set_value('id'),
            'ownerID' => set_value('ownerID'),
            //'UserName' => set_value('userName'),
            'nameOfClub' => set_value('nameOfClub'),
            'price' => set_value('price'),
            'description_attributes' => set_value('description_attributes'),
            // 'validity' => set_value('validity'),
            'discount' => set_value('discount'),
            'image' => set_value('image'),
        );

        $this->load->view('layout/index', $data);
        $this->load->view('package/package_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            //add image
            $imgName = md5(time()). ".png";
            $imgPath = getcwd()."/api/uploads/owner_user/package/".$imgName;

            if (isset($_FILES["image"]["tmp_name"]) && move_uploaded_file($_FILES["image"]["tmp_name"],$imgPath) )
            {
                // echo 'uploaded';die;
            }

            $data = array(
                'ownerID' => $this->input->post('ownerID',TRUE),
                //'UserName' => $this->input->post('userName',TRUE),
                'nameOfClub' => $this->input->post('nameOfClub',TRUE),
                'price' => $this->input->post('price',TRUE),
                'description_attributes' => $this->input->post('description_attributes',TRUE),
                // 'validity' => $this->input->post('validity',TRUE),
                'discount' => $this->input->post('discount',TRUE),
                'image' => $imgName,
            );

            $this->Package_model->insert($data);
            $this->session->set_flashdata('message', 'Created Record Successfully');
            redirect(site_url('package'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Package_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'image_url'=>ORIGINAL_URL2,
                'action' => site_url('package/update_action'),
                'id' => set_value('id', $row->id),
                'ownerID' => set_value('ownerID', $row->ownerID),
                //'UserName' => set_value('UserName', $row->userName),
                'nameOfClub' => set_value('nameOfClub', $row->nameOfClub),
                'price' => set_value('price', $row->price),
                'description_attributes' => set_value('description_attributes', $row->description_attributes),
                'validity' => set_value('validity', $row->validity),
                'discount' => set_value('discount', $row->discount),
                'image' => set_value('image', $row->image),
            );

            $this->load->view('layout/index', $data);
            $this->load->view('package/package_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('package'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {

            //add image
            $filepath= $_SERVER['DOCUMENT_ROOT']. '/api/';

            $imgName = md5(time()). ".png";
            $imgPath = $filepath."uploads/owner_user/package/".$imgName;


            $data = array(
                'ownerID' => $this->input->post('ownerID',TRUE),
                //'UserName' => $this->input->post('userName',TRUE),
                'nameOfClub' => $this->input->post('nameOfClub',TRUE),
                'price' => $this->input->post('price',TRUE),
                'description_attributes' => $this->input->post('description_attributes',TRUE),
                'validity' => $this->input->post('validity',TRUE),
                'discount' => $this->input->post('discount',TRUE),
                // 'image' => "uploads/".$imgName,
            );

            if (isset($_FILES["image"]["tmp_name"]) && move_uploaded_file($_FILES["image"]["tmp_name"],$imgPath) )
            {
                $data['image'] = $imgName;
            }

            $this->Package_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Updated Record Successfully');
            redirect(site_url('package'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Package_model->get_by_id($id);

        if ($row) {
            $this->Package_model->delete($id);
            $this->session->set_flashdata('message', 'Deleted Record Successfully');
            redirect(site_url('package'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('package'));
        }
    }

    public function _rules()
    {

        $this->form_validation->set_rules('ownerID', 'ownerid', 'trim|required');
        // $this->form_validation->set_rules('UserName', 'UserName', 'trim|required');
        $this->form_validation->set_rules('nameOfClub', 'nameofclub', 'trim|required');
        $this->form_validation->set_rules('price', 'price', 'trim|required');
        $this->form_validation->set_rules('description_attributes', 'description attributes', 'trim|required');
        // $this->form_validation->set_rules('validity', 'validity', 'trim|required');
        $this->form_validation->set_rules('discount', 'discount', 'trim|required');
        // $this->form_validation->set_rules('image', 'image', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
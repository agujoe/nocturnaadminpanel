<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Likes extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Likes_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'likes?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'likes?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'likes/';
            $config['first_url'] = base_url() . 'likes/';
        }

        $config['per_page'] = 12;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Likes_model->total_rows($q);
        $likes = $this->Likes_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'likes_data' => $likes,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

        $this->load->view('layout/index', $data);
        $this->load->view('likes/likes_list', $data);

    }

    public function read($id) 
    {
        $row = $this->Likes_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'userID' => $row->userID,
                'statusid' => $row->statusid,
            );

            $this->load->view('layout/index', $data);
            $this->load->view('likes/likes_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('likes'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('likes/create_action'),
            'id' => set_value('id'),
            'userID' => set_value('userID'),
            'statusid' => set_value('statusid'),
        );

        $this->load->view('layout/index', $data);
        $this->load->view('likes/likes_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'userID' => $this->input->post('userID',TRUE),
                'statusid' => $this->input->post('statusid',TRUE),
            );

            $this->Likes_model->insert($data);
            $this->session->set_flashdata('message', 'Record Created Successfully');
            redirect(site_url('likes'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Likes_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('likes/update_action'),
                'id' => set_value('id', $row->id),
                'userID' => set_value('userID', $row->userID),
                'statusid' => set_value('statusid', $row->statusid),
            );

            $this->load->view('layout/index', $data);
            $this->load->view('likes/likes_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('likes'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'userID' => $this->input->post('userID',TRUE),
                'statusid' => $this->input->post('statusid',TRUE),
            );

            $this->Likes_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Record Updated Successfully');
            redirect(site_url('likes'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Likes_model->get_by_id($id);

        if ($row) {
            $this->Likes_model->delete($id);
            $this->session->set_flashdata('message', 'Record Deleted Successfully');
            redirect(site_url('likes'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('likes'));
        }
    }

    public function _rules()
    {

        $this->form_validation->set_rules('userID', 'userid', 'trim|required');
        $this->form_validation->set_rules('statusid', 'statusid', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

    }

}
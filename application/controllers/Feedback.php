<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Feedback extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Feedback_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'feedback?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'feedback?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'feedback/';
            $config['first_url'] = base_url() . 'feedback/';
        }

        $config['per_page'] = 12;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Feedback_model->total_rows($q);
        $feedback = $this->Feedback_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'feedback_data' => $feedback,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

        $this->load->view('layout/index', $data);
        $this->load->view('feedback/feedback_list', $data);

    }

    public function read($id) 
    {
        $row = $this->Feedback_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'userID' => $row->userID,
                'feedback' => $row->feedback,
            );

            $this->load->view('layout/index', $data);
            $this->load->view('feedback/feedback_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('feedback'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('feedback/create_action'),
            'id' => set_value('id'),
            'userID' => set_value('userID'),
            'feedback' => set_value('feedback'),
        );

        $this->load->view('layout/index', $data);
        $this->load->view('feedback/feedback_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'userID' => $this->input->post('userID',TRUE),
                'feedback' => $this->input->post('feedback',TRUE),
            );

            $this->Feedback_model->insert($data);
            $this->session->set_flashdata('message', 'Created Record Successfully');
            redirect(site_url('feedback'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Feedback_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('feedback/update_action'),
                'id' => set_value('id', $row->id),
                'userID' => set_value('userID', $row->userID),
                'feedback' => set_value('feedback', $row->feedback),
            );

            $this->load->view('layout/index', $data);
            $this->load->view('feedback/feedback_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('feedback'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'userID' => $this->input->post('userID',TRUE),
                'feedback' => $this->input->post('feedback',TRUE),
            );

            $this->Feedback_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Updated Record Successfully');
            redirect(site_url('feedback'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Feedback_model->get_by_id($id);

        if ($row) {
            $this->Feedback_model->delete($id);
            $this->session->set_flashdata('message', 'Deleted Record Successfully');
            redirect(site_url('feedback'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('feedback'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('userID', 'userid', 'trim|required');
        $this->form_validation->set_rules('feedback', 'feedback', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

    }

}
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Friends extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Friends_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'friends?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'friends?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'friends/';
            $config['first_url'] = base_url() . 'friends/';
        }

        $config['per_page'] = 12;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Friends_model->total_rows($q);
        $friends = $this->Friends_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'friends_data' => $friends,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

        $this->load->view('layout/index', $data);
        $this->load->view('friends/friends_list', $data);

    }

    public function read($id) 
    {
        $row = $this->Friends_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
		        'userID' => $row->userID,
		        'friendID' => $row->friendID,
		        'status' => $row->status,
            );

            $this->load->view('layout/index', $data);
            $this->load->view('friends/friends_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('friends'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('friends/create_action'),
	        'id' => set_value('id'),
	        'userID' => set_value('userID'),
	        'friendID' => set_value('friendID'),
	        'status' => set_value('status'),
        );

        $this->load->view('layout/index', $data);
        $this->load->view('friends/friends_form', $data);

    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'userID' => $this->input->post('userID',TRUE),
                'friendID' => $this->input->post('friendID',TRUE),
                'status' => $this->input->post('status',TRUE),
            );

            $this->Friends_model->insert($data);
            $this->session->set_flashdata('message', 'Record Created Successfully');
            redirect(site_url('friends'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Friends_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('friends/update_action'),
                'id' => set_value('id', $row->id),
                'userID' => set_value('userID', $row->userID),
                'friendID' => set_value('friendID', $row->friendID),
                'status' => set_value('status', $row->status),
            );

            $this->load->view('layout/index', $data);
            $this->load->view('friends/friends_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('friends'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'userID' => $this->input->post('userID',TRUE),
		'friendID' => $this->input->post('friendID',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Friends_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Record Updated Successfully');
            redirect(site_url('friends'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Friends_model->get_by_id($id);

        if ($row) {
            $this->Friends_model->delete($id);
            $this->session->set_flashdata('message', 'Record Deleted Successfully');
            redirect(site_url('friends'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('friends'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('userID', 'userid', 'trim|required');
	    $this->form_validation->set_rules('friendID', 'friendid', 'trim|required');
	    $this->form_validation->set_rules('status', 'status', 'trim|required');
	    $this->form_validation->set_rules('id', 'id', 'trim');
	    $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}
<div class="right_col" role="main">
    <h2 style="margin-top:0px">Transactions <?php echo $button ?></h2>
    <form action="<?php echo $action; ?>" method="post">
        <div class="form-group">
            <label for="int">ID <?php echo form_error('ownerID') ?></label>
            <input type="text" class="form-control" name="userID" id="userID" placeholder="UserID" value="<?php echo $userID; ?>" />
        </div>
	    <!-- <div class="form-group">
            <label for="int">PackageID <?php //echo form_error('packageID') ?></label>
            <input type="text" class="form-control" name="packageID" id="packageID" placeholder="PackageID" value="<?php //echo $packageID; ?>" />
        </div> -->
	   <!--  <div class="form-group">
            <label for="varchar">Qr <?php //echo form_error('qr') ?></label>
            <input type="text" class="form-control" name="qr" id="qr" placeholder="Qr" value="<?php //echo $qr; ?>" />
        </div>  -->

        <div class="form-group">
            <label for="varchar">Name of Club<?php echo form_error('nameOfClub') ?></label>
            <input type="text" class="form-control" name="clubname" id="club" placeholder="club name" value="<?php echo $club; ?>" />
        </div>

        <div class="form-group">
            <label for="varchar">Amount<?php echo form_error('price') ?></label>
            <input type="number" class="form-control" name="amount" id="amount" placeholder="amount" value="<?php echo $amount; ?>" />
        </div>

        <div class="form-group">
            <label for="varchar">Date & Time<?php echo form_error('date') ?></label>
            <input type="date" class="form-control" name="date" id="date" placeholder="Date" value="<?php echo $date; ?>" />
        </div>

	   <!--  <div class="form-group">
            <label for="int">Type <?php //echo form_error('type') ?></label>
            <input type="text" class="form-control" name="type" id="type" placeholder="Type" value="<?php //echo $type; ?>" />
        </div> -->

        <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
        <a href="<?php echo site_url('purchased') ?>" class="btn btn-default">Cancel</a>
    </form>
</div>
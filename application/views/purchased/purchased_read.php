<div class="right_col" role="main">
    <h2 style="margin-top:0px">Read</h2>
    <table class="table table-bordered">
        <tr><td>UserID</td><td><?php echo $ownerID; ?></td></tr>
	    <tr><td>NameofClub</td><td><?php echo $nameOfClub; ?></td></tr>
	    <tr><td>Amount</td><td><?php echo $price; ?></td></tr>
	    <tr><td>DateTime</td><td><?php echo $date; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('purchased')?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
</div>
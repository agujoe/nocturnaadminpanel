<div class="right_col" role="main">
        <h2 style="margin-top:0px">Feedback <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">ID <?php echo form_error('userID') ?></label>
            <input type="text" class="form-control" name="userID" id="userID" placeholder="UserID" value="<?php echo $userID; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Feedback <?php echo form_error('feedback') ?></label>
            <input type="text" class="form-control" name="feedback" id="feedback" placeholder="Feedback" value="<?php echo $feedback; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('feedback') ?>" class="btn btn-default">Cancel</a>
	</form>
   </div>
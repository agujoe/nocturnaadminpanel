<div class="right_col" role="main">
        <h2 style="margin-top:0px">Post <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	    <div class="form-group">
            <label for="varchar">ID <?php echo form_error('userID') ?></label>
            <input type="text" class="form-control" name="userID" id="userID" placeholder="UserID" value="<?php echo $userID; ?>" />
        </div>
	    <!-- <div class="form-group">
            <label for="picture">Picture <?php //echo form_error('picture') ?></label>
            <textarea class="form-control" rows="3" name="picture" id="picture" placeholder="Picture"><?php //echo $picture; ?></textarea>
        </div> -->


        <?php
            //When create
            if($button=="Create")
            {
                ?>

            <div class="form-group">
            <label for="picture">Picture <?php echo form_error('picture') ?></label>
            <input type="file" class="form-control" rows="3" name="picture" id="picture" placeholder="Picture" value="<?php echo $picture; ?>" />
            </div>

            <?php

            }
            //when update
            else if($button=="Update")
            {
                ?>

                  <div class="form-group">
            <label for="picture">ProfilePicture <?php echo form_error('Picture') ?></label>
            <input type="file" class="form-control" rows="3" name="picture" id="picture" /><br>
            <img src="<?php echo $image_url.$picture; ?>" id="showImg" width="200px !important" height="150px !important" />

                </div>

            <script>
            
            function readURL(input) {
           
            if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#showImg').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
            }
            
            }
    
            $("#picture").change(function(){
            readURL(this);

            });

            </script> 


            <?php

            }

            ?>


	    <div class="form-group">
            <label for="varchar">Status <?php echo form_error('status') ?></label>
            <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">CheckIn <?php echo form_error('checkIn') ?></label>
            <input type="text" class="form-control" name="checkIn" id="checkIn" placeholder="CheckIn" value="<?php echo $checkIn; ?>" />
        </div>
	    <!-- <div class="form-group">
            <label for="video">Video <?php //echo form_error('video') ?></label>
            <textarea class="form-control" rows="3" name="video" id="video" placeholder="Video"><?php //echo $video; ?></textarea>
        </div> -->


        
        <?php
            //When create
            if($button=="Create")
            {
                ?>

            <div class="form-group">
            <label for="video">Video <?php echo form_error('video') ?></label>
            <input type="file" class="form-control" rows="3" name="video" id="video" placeholder="Video" value="<?php echo $video; ?>" />
            </div>

            <?php

            }
            //when update
            else if($button=="Update")
            {
                ?>

                  <div class="form-group">
            <label for="video">Video <?php echo form_error('video') ?></label>
            <input type="file" class="form-control" rows="3" name="video" id="video" /><br>
            <video width="320" height="150" controls  id="showImg">
             <source src="<?php echo $video_url.$video; ?>" type="video/mp4">
              <source src="<?php echo $video_url.$video; ?>" type="video/ogg">
                Your browser does not support the video tag.
           </video>
           <!-- <img //src="<?php //echo $image_url.$video; ?>" <!--id="showImg" width="200px !important" height="100px !important" />
-->
                </div>

             <script>
            
            function readURL(input) {
           
            if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#showImg').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
            }
            
            }
    
            $("#video").change(function(){
            readURL(this);

            });

             </script> 


            <?php

            }

            ?>


	    <div class="form-group">
            <label for="enum">StatusType <?php echo form_error('statusType') ?></label>
            <input type="text" class="form-control" name="statusType" id="statusType" placeholder="StatusType" value="<?php echo $statusType; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Date <?php echo form_error('date') ?></label>
            <input type="text" class="form-control" name="date" id="datetimepicker" placeholder="Date" value="<?php echo $date; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('post') ?>" class="btn btn-default">Cancel</a>
	</form>
    </div>

    <script>
  
        jQuery('#datetimepicker').datetimepicker();

    </script>
<div class="right_col" role="main">
    <h2 style="margin-top:0px">Post</h2>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-md-4">
            <!-- <?php //echo anchor(site_url('post/create'),'Create', 'class="btn btn-primary"'); ?> -->
        </div>
        <div class="col-md-4 text-center">
            <div style="margin-top: 8px" id="message">
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
            </div>
        </div>
        <div class="col-md-1 text-right"></div>
        <div class="col-md-3 text-right">
            <form action="<?php echo site_url('post/index'); ?>" class="form-inline" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                    <span class="input-group-btn">
                        <?php
                        if ($q <> '')
                        {
                            ?>
                            <a href="<?php echo site_url('post'); ?>" class="btn btn-default">Reset</a>
                            <?php
                        }
                        ?>
                        <button class="btn btn-primary" type="submit">Search</button>
                    </span>
                </div>
            </form>
        </div>
    </div>

    <table class="table table-bordered" style="margin-bottom: 10px">
        <tr style="background-color: #4C3078; color:#ffffff;">
            <th>No</th>
            <th>ID</th>
            <th>Status</th>
            <th>CheckIn</th>
            <th>StatusType</th>
            <th>Picture</th>
            <th>Date</th>
            <th style="width:13%">Action</th>
        </tr>
        <?php
        foreach ($post_data as $post)
        {
            ?>
            <tr>
                <td width="80px"><?php echo ++$start ?></td>
                <td>
                    <?php echo $post->userID ?>
                </td>
                <td><?php echo $post->status ?></td>
                <td><?php echo $post->checkIn ?></td>
                <!--<td><?php /*//echo $post->video */?>
                    <video width="320" height="150" controls>
                        <source src="<?php /*//echo $video_url.$post->video; */?>" type="video/mp4">
                        <source src="<?php /*//echo $video_url.$post->video; */?>" type="video/ogg">
                        Your browser does not support the video tag.
                    </video>
                </td>-->

                <td><?php echo $post->statusType ?></td>
                <td>
                    <a href="<?php echo base_url($post->picture) ?>">Click to Check Profile Picture</a>
                    <!-- <img src="<?php /*/*echo $image_url */?>" width="200px" height="150px" onerror="this.src = 'http://nocturnaapp.com/adminpanel/noimage.png';">-->
                </td>
                <td><?php echo $post->date ?></td>

                <td>
                    <div class="col-md-12">
                        <div class="btn-group">
                            <a href="<?php echo base_url('post/read/'.$post->id);?>"><button class="btn btn-sm btn-success" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Read"><i class="fa fa-book"></i></button></a>
                            <a href="<?php echo base_url('post/update/'.$post->id);?>"><button class="btn btn-sm btn-primary" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Update"><i class="fa fa-edit"></i></button></a>
                            <a href="<?php echo base_url('post/delete/'.$post->id);?>"><button class="btn btn-sm btn-danger" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></button></a>
                        </div>
                    </div>
                </td>
            </tr>

            <?php
        }
        ?>
    </table>

    <div class="row">
        <div class="col-md-6">
            <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	    </div>
        <div class="col-md-6 text-right">
            <?php echo $pagination ?>
        </div>
    </div>
</div>
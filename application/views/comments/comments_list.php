<div class="right_col" role="main">
        <h2 style="margin-top:0px">Comments</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('comments/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('comments/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('comments'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr style=" background-color: #4C3078; color: #ffffff;">
                <th>No</th>
                <th>ID</th>
                <th>Statusid</th>
                <th>Comment</th>
                <th style="width:13%">Action</th>
            </tr>
            <?php
            foreach ($comments_data as $comments)
            {
                ?>
                <tr>
                    <td width="80px"><?php echo ++$start ?></td>
                    <td><?php echo $comments->userID ?></td>
                    <td><?php echo $comments->statusid ?></td>
                    <td><?php echo $comments->comment ?></td>
                    <td>
                        <div class="col-md-12">
                            <div class="btn-group">
                                <a href="<?php echo base_url('comments/read/'.$comments->id);?>"><button class="btn btn-sm btn-success" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Read"><i class="fa fa-book"></i></button></a>
                                <a href="<?php echo base_url('comments/update/'.$comments->id);?>"><button class="btn btn-sm btn-primary" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Update"><i class="fa fa-edit"></i></button></a>
			                    <a href="<?php echo base_url('comments/delete/'.$comments->id);?>"><button class="btn btn-sm btn-danger" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></button></a>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
    <div class="row">
        <div class="col-md-6">
            <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
        </div>
        <div class="col-md-6 text-right">
            <?php echo $pagination ?>
        </div>
    </div>
</div>
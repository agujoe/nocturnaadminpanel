<div class="right_col" role="main">
     <h2 style="margin-top:0px">Comments<?php echo $button ?></h2>
    <form action="<?php echo $action; ?>" method="post">
        <div class="form-group">
            <label for="int">ID <?php echo form_error('userID') ?></label>
            <input type="text" class="form-control" name="userID" id="userID" placeholder="UserID" value="<?php echo $userID; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Statusid <?php echo form_error('statusid') ?></label>
            <input type="text" class="form-control" name="statusid" id="statusid" placeholder="Statusid" value="<?php echo $statusid; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Comment <?php echo form_error('comment') ?></label>
            <input type="text" class="form-control" name="comment" id="comment" placeholder="Comment" value="<?php echo $comment; ?>" />
        </div>
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
        <a href="<?php echo site_url('comments') ?>" class="btn btn-default">Cancel</a>
    </form>
</div>

<div class="row">
    <div class="col-md-3">

    </div>

    <div class="col-md-12">
        <div class="right_col" role="main">
            <br><br>
            <h2 style="margin-top:0px">Create New Friends <?php //echo $button ?></h2>
            <form action="<?php echo $action; ?>" method="post">
                <div class="form-group">
                    <label for="int">ID <?php echo form_error('userID') ?></label>
                    <input type="text" class="form-control" name="userID" id="userID" placeholder="UserID" value="<?php echo $userID; ?>" />
                </div>

                <div class="form-group">
                    <label for="int">FriendID <?php echo form_error('friendID') ?></label>
                    <input type="text" class="form-control" name="friendID" id="friendID" placeholder="FriendID" value="<?php echo $friendID; ?>" />
                </div>

                <div class="form-group">
                    <label for="int">Status <?php echo form_error('status') ?></label>
                    <select class="form-control" name="status" id="request"required>
                        <option value="" disabled selected>Status</option>
                        <option value="accept" id="status">Accept</option>
                        <option value="pending" id="status">Pending</option>
                    </select>
                    <span id="" class="form-error"></span>
                </div>

                <!--<div class="form-group">
                    <label for="enum">Status <?php /*echo form_error('status') */?></label>
                    <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php /*echo $status; */?>" />
                </div>-->

                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                <a href="<?php echo site_url('friends') ?>" class="btn btn-default">Cancel</a>
            </form>
        </div>
    </div>
</div>

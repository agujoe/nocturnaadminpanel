<?php
?>
<html>
<head>
    <style type="text/css"></style>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/bootstrap/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/css/login.css">

</head>

<body style="background-color: #f0f0f0">
<div class="container" >
    <br><br><br><br><br><br>
    <div class="row border" id=color_white>
        <div class="col-md-6 border" id="image_column">
            <img class="img-responsive" id="logo"  src="<?php echo base_url()?>/public/images/logo.png" >
        </div>
        <div class="col-md-4" id="main">
            <div class="card">
                <div class="card-header" id="color_white">
                    <h4 align="center">Login</h4>
                </div>
                <!--Display error message if login details is incorrect-->
                <?php
                if($this->session->error){
                    $error = $this->session->error;
                    echo '<div class="alert-danger text-center">
                        <h6>'.$error.'</h6> 
                      </div>';
                }  ?>
                <div class="card-body">
                    <form method="post" action="admin/login">
                        <div class="form-group">
                            <label>Username:</label>
                            <input class="border" type="text" name="username" id="name" placeholder="username" required>
                        </div>
                        <div class="form-group">
                            <label>Password:</label>
                            <input class="border" type="password" name="password"  id="password" placeholder="***********" required>
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="gridCheck">
                                <label class="form-check-label" for="gridCheck">
                                    Remember me
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="border btn btn-group-lg" id="btn_style" type="submit">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo base_url()?>/public/bootstrap/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>


</body>
</html>
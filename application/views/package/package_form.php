<div class="right_col" role="main">
    <h2 style="margin-top:0px">Package <?php echo $button ?></h2>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	    <div class="form-group">
            <label for="int">OwnerID <?php echo form_error('ownerID') ?></label>
            <input type="text" class="form-control" name="ownerID" id="ownerID" placeholder="OwnerID" value="<?php echo $ownerID; ?>" />
        </div>
       <!--  <div class="form-group">
            <label for="varchar">UserName <?php //echo form_error('userName') ?></label>
            <input type="text" class="form-control" name="UserName" id="UserName" placeholder="UserName" value="<?php //echo $userName; ?>" />
        </div> -->
	    <div class="form-group">
            <label for="varchar">NameOfClub <?php echo form_error('nameOfClub') ?></label>
            <input type="text" class="form-control" name="nameOfClub" id="nameOfClub" placeholder="NameOfClub" value="<?php echo $nameOfClub; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Price <?php echo form_error('price') ?></label>
            <input type="text" class="form-control" name="price" id="price" placeholder="Price" value="<?php echo $price; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Description Attributes <?php echo form_error('description_attributes') ?></label>
            <input type="text" class="form-control" name="description_attributes" id="description_attributes" placeholder="Description Attributes" value="<?php echo $description_attributes; ?>" />
        </div>
	   <!--  <div class="form-group">
            <label for="varchar">Validity <?php //echo form_error('validity') ?></label>
            <input type="text" class="form-control" name="validity" id="validity" placeholder="Validity" value="<?php //echo $validity; ?>" />
        </div> -->
	    <div class="form-group">
            <label for="varchar">Discount <?php echo form_error('discount') ?></label>
            <input type="text" class="form-control" name="discount" id="discount" placeholder="Discount" value="<?php echo $discount; ?>" />
        </div>
	 <!--    <div class="form-group">
            <label for="image">Image <?php //echo form_error('image') ?></label>
            <textarea class="form-control" rows="3" name="image" id="image" placeholder="Image"><?php //echo $image; ?></textarea>
        </div> -->

        <?php
        //When create
        if($button=="Create")
        {
            ?>

            <div class="form-group">
                <label for="image">Image <?php echo form_error('image') ?></label>
                <input type="file" class="form-control" rows="3" name="image" id="image" placeholder="image" value="<?php echo $image; ?>" />
            </div>

        <?php

        }
        //when update
        else if($button=="Update")
        {
            ?>

            <div class="form-group">
                <label for="image">Image <?php echo form_error('image') ?></label>
                <input type="file" class="form-control" rows="3" name="image" id="image" /><br>
                <img src="<?php echo $image_url.$image; ?>" id="showImg" width="200px !important" height="100px !important" />
            </div>

            <script>
                function readURL(input) {

                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $('#showImg').attr('src', e.target.result);
                        };

                        reader.readAsDataURL(input.files[0]);
                    }
                }

                $("#image").change(function(){
                    readURL(this);

                });

            </script>

            <?php
        }
        ?>

        <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
	    <a href="<?php echo site_url('package') ?>" class="btn btn-default">Cancel</a>
	</form>
</div>
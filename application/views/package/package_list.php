 <div class="right_col" role="main">
     <h2 style="margin-top:0px">Package</h2>
     <div class="row" style="margin-bottom: 10px">
         <div class="col-md-4">
             <?php echo anchor(site_url('package/create'),'Create', 'class="btn btn-primary"'); ?>
         </div>

         <div class="col-md-4 text-center">
             <div style="margin-top: 8px" id="message">
                 <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
             </div>
         </div>
         <div class="col-md-1 text-right"></div>
         <div class="col-md-3 text-right">
             <form action="<?php echo site_url('package/index'); ?>" class="form-inline" method="get">
                 <div class="input-group">
                     <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                     <span class="input-group-btn">
                         <?php
                         if ($q <> '')
                         {
                             ?>
                             <a href="<?php echo site_url('package'); ?>" class="btn btn-default">Reset</a>
                             <?php
                         }
                         ?>
                         <button class="btn btn-primary" type="submit">Search</button>
                     </span>
                 </div>
             </form>
         </div>
     </div>

     <table class="table table-bordered" style="margin-bottom: 10px">
         <tr style="background-color: #4c177d; color:#ffffff;">
             <th>No</th>
             <th>OwnerID</th>
             <!-- <th>UserName</th> -->
             <th>NameOfClub</th>
             <th>Price</th>
             <th>Description Attributes</th>
             <!-- <th>Validity</th> -->
             <th>Discount</th>
             <!--<th>Image</th>-->
             <th style="width:13%">Action</th>
         </tr>
         <?php
         foreach ($package_data as $package)
         {
             ?>
             <td>
                 <td width="80px"><?php echo ++$start ?></td>
                 <td><?php echo $package->ownerID ?></td>
                 <!-- <td><?php //echo $package->userName ?></td>-->
                 <td><?php echo $package->nameOfClub ?></td>
                 <td><?php echo $package->price ?></td>
                 <td><?php echo $package->description_attributes ?></td>
                 <!--<td><?php //echo $package->validity ?></td>-->
                 <!--<td><?php /*//echo $package->discount */?></td>-->
                 <!--<td><?php /*//echo $package->image */?>
                     <img src="<?php /*//echo $image_url.$package->image; */?>" width="200px" height="100px" onerror="this.src = ''http://nocturnaapp.com/adminpanel/noimage.png'';"></td>
                 <td>-->

             <td>
                 <div class="col-md-12">
                     <div class="btn-group">
                         <a href="<?php echo base_url('package/read/'.$package->id);?>"><button class="btn btn-sm btn-success" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Read"><i class="fa fa-book"></i></button></a>
                         <a href="<?php echo base_url('package/update/'.$package->id);?>"><button class="btn btn-sm btn-primary" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Update"><i class="fa fa-edit"></i></button></a>
                         <a href="<?php echo base_url('package/delete/'.$package->id);?>"><button class="btn btn-sm btn-danger" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></button></a>
                     </div>
                 </div>
             </td>
             </tr>

             <?php
         }
         ?>
     </table>
     <div class="row">
         <div class="col-md-6">
             <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	     </div>

         <div class="col-md-6 text-right">
             <?php echo $pagination ?>
         </div>
     </div>
 </div>
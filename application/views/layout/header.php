<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Nocturna App</title>

    <!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/dist/css/bootstrap.min.css') ?>"/>
    
    <!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css') ?>"/>
    
    <!-- NProgress -->
    <link rel="stylesheet" href="<?php echo base_url('assets/nprogress/nprogress.css') ?>"/>
    <!-- iCheck -->
   	<link rel="stylesheet" href="<?php echo base_url('assets/iCheck/skins/flat/green.css') ?>"/>
    <!-- bootstrap-progressbar -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') ?>"/>
    <!-- JQVMap -->
	<link rel="stylesheet" href="<?php echo base_url('assets/jqvmap/dist/jqvmap.min.css') ?>"/>

    <!-- Custom Theme Style -->
<!--
    <link rel="stylesheet" href="<?php /*echo base_url('assets/datatables.net-bs/css/dataTables.bootstrap.min.css') */?>"/>
	<link rel="stylesheet" href="<?php /*echo base_url('assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') */?>"/>
	<link rel="stylesheet" href="<?php /*echo base_url('assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') */?>"/>
	<link rel="stylesheet" href="<?php /*echo base_url('assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') */?>"/>
	<link rel="stylesheet" href="<?php /*echo base_url('assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') */?>"/>
	
    <link rel="stylesheet" type="text/css" href="<?php /*echo base_url('assets/datetimepicker-master/jquery.datetimepicker.css') */?>"/ >
    <script src="<?php /*echo base_url('assets/datetimepicker-master/jquery.js') */?>"></script>
    <script src="<?php /*echo base_url('assets/datetimepicker-master/build/jquery.datetimepicker.full.min.js') */?>"></script>
-->
    <link rel="stylesheet" href="<?php echo base_url('public/css/custom.min.css') ?>"/>
</head>

<body class="nav-md">
<div class="container body">
        <div class="main_container">
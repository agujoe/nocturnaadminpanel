<footer>
    <div class="pull-right">
        <b><a href="http://nocturnaapp.com/" target="_blank">Nocturna App.</a></b>
    </div>
    <div class="clearfix"></div>
</footer>

</div>
</div>


<!--Import D3.js plugin-->
<script src="https://d3js.org/d3.v5.min.js"></script>

<!-- jQuery -->
<script src="<?php echo base_url('assets/jquery/dist/jquery.min.js') ?>"></script>

<!-- Bootstrap -->
<script src="<?php echo base_url('assets/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/fastclick/lib/fastclick.js') ?>"></script>
<!-- NProgress -->
<script src="<?php echo base_url('assets/nprogress/nprogress.js') ?>"></script>

<!-- bootstrap-progressbar -->
<script src="<?php echo base_url('assets/bootstrap-progressbar/bootstrap-progressbar.min.js') ?>"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/iCheck/icheck.min.js') ?>"></script>
<!-- Skycons -->
<script src="<?php echo base_url('assets/skycons/skycons.js') ?>"></script>
<!-- Float -->
<script src="<?php echo base_url('assets/Flot/jquery.flot.js') ?>"></script>

<script src="<?php echo base_url('assets/Flot/jquery.flot.pie.js') ?>"></script>
<script src="<?php echo base_url('assets/Flot/jquery.flot.time.js') ?>"></script>
<script src="<?php echo base_url('assets/Flot/jquery.flot.stack.js') ?>"></script>
<script src="<?php echo base_url('assets/Flot/jquery.flot.resize.js') ?>"></script>
<!-- Flot plugins -->

<script src="<?php echo base_url('assets/flot.orderbars/js/jquery.flot.orderBars.js') ?>"></script>
<script src="<?php echo base_url('assets/flot-spline/js/jquery.flot.spline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/flot.curvedlines/curvedLines.js') ?>"></script>
  
<!-- DateJS -->
<script src="<?php echo base_url('assets/DateJS/build/date.js') ?>"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo base_url('public/js/custom.min.js') ?>"></script>

</body>
</html>
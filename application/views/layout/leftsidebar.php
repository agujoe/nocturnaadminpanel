<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title">
            <a href="<?php echo base_url()."welcome"; ?>">
                <img src="<?php echo base_url('public/images/nocturna.png"') ?>"/>
            </a>
        </div>

        <div class="profile">
            <div class="profile_pic">
                <img src="<?php echo base_url('public/images/avatar.jpeg" alt="..." class="img-circle profile_img') ?>"/>
            </div>
            <div class="profile_info">
                <span>Welcome </span>
                <h2>Name: </h2>
            </div>
        </div>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-home" aria-hidden="false"></i> Dashboard <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo base_url()."admin/home"; ?>"> Home </a> </li>
                        </ul>
                    </li>

                    <li><a><i class="fa fa-user" aria-hidden="false"></i> Normaluser
                            <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo base_url('normaluser');?>"><i class="fa fa-users"></i> All Users</a></li>
                            <li><a href="<?php echo base_url('Friends'); ?>"><i class="fa fa-users"></i> Friends</a></li>
                            <li><a href="<?php echo base_url('Post'); ?>"><i class="fa fa-tag" aria-hidden="true"></i>Post</a></li>
                            <li><a href="<?php echo base_url('Comments'); ?>"><i class="fa fa-comments" aria-hidden="true"></i></i>Comments</a></li>
                            <li><a href="<?php echo base_url('Likes'); ?>"><i class="fa fa-thumbs-up" aria-hidden="true"></i>Likes</a></li>
                            <li><a href="<?php echo base_url('Purchased'); ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Transactions</a></li>
                        </ul>
                    </li>

                    <li><a><i class="fa fa-user" aria-hidden="false"></i> Place Owner
                            <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo base_url('Owneruser'); ?>"><i class="fa fa-user" aria-hidden="true"></i>Place Owner</a></li>
                            <li><a href="<?php echo base_url('Package'); ?>"><i class="fa fa-suitcase" aria-hidden="true"></i>Package</a></li>
                        </ul>

                    </li>

                    <li><a href="<?php echo base_url('Feedback');?>"><i class="fa fa-comment" aria-hidden="false"></i> Feedback</a></li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a href="<?php echo base_url('')?>" data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>

        <!-- /menu footer buttons -->

    </div>
</div>
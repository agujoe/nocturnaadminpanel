 <div class="right_col" role="main">
     <h2 style="margin-top:0px">Likes<?php echo $button ?></h2>
     <form action="<?php echo $action; ?>" method="post">
         <div class="form-group">
             <label for="int">ID <?php echo form_error('userID') ?></label>
             <input type="text" class="form-control" name="userID" id="userID" placeholder="UserID" value="<?php echo $userID; ?>" />
         </div>

         <div class="form-group">
             <label for="int">Statusid <?php echo form_error('statusid') ?></label>
             <input type="text" class="form-control" name="statusid" id="statusid" placeholder="Statusid" value="<?php echo $statusid; ?>" />
         </div>

         <input type="hidden" name="id" value="<?php echo $id; ?>" />
         <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
         <a href="<?php echo site_url('likes') ?>" class="btn btn-default">Cancel</a>
     </form>
 </div>
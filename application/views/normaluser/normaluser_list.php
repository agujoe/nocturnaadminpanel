<div class="right_col" role="main">
    <h2 style="margin-top:0px">Normaluser</h2>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-md-4">
            <?php echo anchor(base_url('normaluser/create'),'Create', 'class="btn"');?>
        </div>

        <div class="col-md-4 text-center">
            <div style="margin-top: 8px" id="message">
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
            </div>
        </div>

        <div class="col-md-1 text-right"></div>
        <div class="col-md-3 text-right">
            <form action="<?php echo site_url('normaluser/index'); ?>" class="form-inline" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                    <span class="input-group-btn">
                        <?php
                        if ($q <> '') {
                            ?>
                            <a href="<?php echo site_url('NormalUser'); ?>" class="btn btn-default">Reset</a>
                            <?php
                        } ?>
                        <button class="btn" type="submit">Search</button>
                    </span>
                </div>
            </form>
        </div>
    </div>

    <table class="table" style="margin-bottom: 10px">
        <thead>
        <tr style="background-color: #4C3078; color:#ffffff;">
            <th>No</th>
            <th>User ID</th>
            <th>UserName</th>
            <th>FullName</th>
            <th>Email</th>
            <th>Password</th>
            <th>PhoneNumber</th>
            <th>TypeDevice</th>
            <th style="width:14% !important">Action</th>
        </tr>
        </thead>

        <?php
        foreach ($normaluser_data as $normaluser) {
            ?>
            <tr>
                <td width="80px"><?php echo ++$start ?></td>
                <td><?php echo $normaluser->id ?></td>
                <td><?php echo $normaluser->userName ?></td>
                <td><?php echo $normaluser->fullName ?></td>
                <td><?php echo $normaluser->email ?></td>
                <td><?php echo $normaluser->password ?></td>
                <td><?php echo $normaluser->phoneNumber ? : "No number" ?></td>
                <td><?php echo $normaluser->typeDevice ?></td>
                <td>
                    <div class="col-md-12">
                        <div class="btn-group">
                            <a href="<?php echo base_url('normaluser/read/'.$normaluser->id);?>"><button class="btn btn-sm btn-success" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Read"><i class="fa fa-book"></i></button></a>
                            <a href="<?php echo base_url('normaluser/update/'.$normaluser->id);?>"><button class="btn btn-sm btn-primary" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Update"><i class="fa fa-edit"></i></button></a>
                            <a href="<?php echo base_url('normaluser/delete/'.$normaluser->id);?>"><button class="btn btn-sm btn-danger" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></button></a>
                        </div>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
        </table>

    <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn">Total Record : <?php echo $total_rows ?></a>
            </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    </div>
<?php //die("did you get here ?"); ?>

<div class="row">
    <div class="col-md-5"></div>
    <div class="col-md-4">

        <br><br>
        <div class="right-col" role="main">
            <h2 style="margin-top:0px">Normaluser <?php echo $button ?></h2>

            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

                <!--<div class="form-group">
                    <label for="varchar">GooglePlus <?php /*//echo form_error('googlePlus') */?></label>
                    <input type="text" class="form-control" name="googlePlus" id="googlePlus" placeholder="GooglePlus" value="<?php /*//echo $googlePlus; */?>" />
                </div>

                <div class="form-group">
                    <label for="varchar">FaceBook <?php /*//echo form_error('faceBook') */?></label>
                    <input type="text" class="form-control" name="faceBook" id="faceBook" placeholder="FaceBook" value="<?php /*//echo $faceBook; */?>" />
                </div>-->
                <div class="form-group">
                    <label for="varchar">UserName <?php echo form_error('userName') ?></label>
                    <input type="text" class="form-control" name="userName" id="userName" placeholder="UserName" value="<?php echo $userName; ?>" />
                </div>
                <div class="form-group">
                    <label for="varchar">FullName <?php echo form_error('fullName') ?></label>
                    <input type="text" class="form-control" name="fullName" id="fullName" placeholder="FullName" value="<?php echo $fullName; ?>" />
                </div>

                <!--<div class="form-group">
                    <label for="int">Age <?php /*//echo form_error('age') */?></label>
                    <input type="text" class="form-control" name="age" id="age" placeholder="Age" value="<?php /*//echo $age; */?>" />
                </div>
                <div class="form-group">
                    <label for="varchar">Gender <?php /*//echo form_error('gender') */?></label>
                    <input type="text" class="form-control" name="gender" id="gender" placeholder="Gender" value="<?php /*//echo $gender; */?>" />
                </div>
                <div class="form-group">
                    <label for="varchar">City <?php /*//echo form_error('city') */?></label>
                    <input type="text" class="form-control" name="city" id="city" placeholder="City" value="<?php /*//echo $city; */?>" />
                </div> -->

                <div class="form-group">
                    <label for="varchar">Email <?php echo form_error('email') ?></label>
                    <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
                </div>
                <div class="form-group">
                    <label for="varchar">Password <?php echo form_error('password') ?></label>
                    <input type="text" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
                </div>
                <div class="form-group">
                    <label for="varchar">PhoneNumber <?php echo form_error('phoneNumber') ?></label>
                    <input type="text" class="form-control" name="phoneNumber" id="phoneNumber" placeholder="PhoneNumber" value="<?php echo $phoneNumber; ?>" />
                </div>

                 <!--<div class="form-group">
                     <label for="varchar">Intrest <?php /*//echo form_error('intrest') */?></label>
                     <input type="text" class="form-control" name="intrest" id="intrest" placeholder="Intrest" value="<?php /*//echo $intrest; */?>" />
                 </div>
                <div class="form-group">
                    <label for="varchar">Dis <?php /*//echo form_error('dis') */?></label>
                    <input type="text" class="form-control" name="dis" id="dis" placeholder="Dis" value="<?php /*//echo $dis; */?>" />
                </div>
                <div class="form-group">
                    <label for="varchar">DeviceToken <?php /*//echo form_error('deviceToken') */?></label>
                    <input type="text" class="form-control" name="deviceToken" id="deviceToken" placeholder="DeviceToken" value="<?php /*//echo $deviceToken; */?>" />
                </div>-->
                <div class="form-group">
                    <label for="varchar">Device Type <?php /*//echo form_error('typeDevice') */?></label>
                    <input type="text" class="form-control" name="typeDevice" id="typeDevice" placeholder="TypeDevice" value="<?php /*//echo $typeDevice; */?>" />
                </div>

                <?php //echo form_error('profilePicture') ?></label>
<!--                <textarea class="form-control" rows="3" name="profilePicture" id="profilePicture" placeholder="ProfilePicture">-->
                    <?php //echo $profilePicture; ?></textarea>

                <?php
                //When create
                if($button=="Create")
                { ?>
                    <div class="form-group">
                        <label for="profilePicture">ProfilePicture <?php echo form_error('profilePicture') ?></label>
                        <input type="file" class="form-control" rows="3" name="profilePicture" id="profilePicture" placeholder="ProfilePicture" value="<?php echo $profilePicture; ?>" />
                    </div>

                <?php
                }
                //when update
                else if($button=="Update")
                {
                    ?>

                    <div class="form-group">
                        <label for="profilePicture">ProfilePicture <?php echo form_error('profilePicture') ?></label>
                        <input type="file" class="form-control" rows="3" name="profilePicture" id="profilePicture" /><br>
                        <img src="<?php echo $image_url.$profilePicture; ?>" id="showImg" width="200px !important" height="100px !important" />
                    </div>

                    <script>

                        function readURL(input) {

                            if (input.files && input.files[0]) {
                                var reader = new FileReader();

                                reader.onload = function (e) {
                                    $('#showImg').attr('src', e.target.result);
                                };

                                reader.readAsDataURL(input.files[0]);
                            }
                        }

                        $("#profilePicture").change(function(){
                            readURL(this);
                        });
                    </script>

                    <?php
                }?>

                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                <a href="<?php echo site_url('NormalUser') ?>" class="btn btn-default">Cancel</a>
            </form>
        </div>
    </div>
</div>
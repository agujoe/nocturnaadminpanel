<?php
include "layout/index.php";
?>
<!-- page content -->
    <div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <!--Row-->
        <div class="row">
            <!--Col-->
            <div class="col-md-12 col-sm-12 col-xs-12">

                <!--Row-->
                <div class="row tile_count">
                    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="glyphicon glyphicon-user"></i> Normal User </span>
                        <div class="count"><?php echo $normalUserCount; ?></div>
					</div>
                    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="glyphicon glyphicon-shopping-cart"></i> Carts </span>
                        <div class="count"><?php echo $cartCount; ?></div>
                    </div>

                    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="glyphicon glyphicon-bed"></i> Purchased </span>
                        <div class="count"><?php echo $purchaseCount; ?></div>
                    </div>

                    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="glyphicon glyphicon-user"></i> Place Owner </span>
                        <div class="count"><?php echo $placeOwnerCount; ?></div>
					</div>
                    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="glyphicon glyphicon-plus"></i> All Packages </span>
                        <div class="count"><?php echo $packageCount; ?></div>
					</div>

                    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="glyphicon glyphicon-pencil"></i> Post </span>
                        <div class="count"><?php echo $postCount; ?></div>
					</div>
				  </div>
            </div>
             <!-- /col -->
        </div>
         <!-- /row -->

        <!--Row, bar chart between Normal User and Post Frequency -->
        <div class="row">
        	<div class="col-md-9 col-xs-12">
              	<div class="x_panel tile fixed_height_320">
                	<div class="x_title">
                  		<h2>Normal User against Post frequency</h2>
                        <ul class="nav navbar-right panel_toolbox"></ul>
                  		<div class="clearfix">
                            <h5>(User's Post Frequency)</h5>
                        </div>
                	</div>
	                <div class="x_content">
                        <div class="widget_summary">
                            <div class="w_left w_25">
                                <span></span>
                            </div>
                            <div class="w_center w_55">
                                <div>
                                    <canvas id="postFrequency" width="150" height="110"></canvas>
                                </div>
                                <br><br>
                            </div>
                            <div class="w_right w_20">
                                <span></span>
                            </div>
                        </div>
                    </div> <!-- /content -->
                </div> <!-- /panel -->
            </div> <!-- /col -->

            <!--Column shows which device is mostly used-->
            <div class="col-md-3 col-xs-12">
              	<div class="x_panel tile fixed_height_320">
                	<div class="x_title">
                        <h2>Devices Most Used (Android and iOS)</h2>
                        <div class="clearfix">
                            <div class="container"></div>
                        </div>
                	</div>
                	<br><br>
	                <div class="x_content">
                        <div class="widget_summary">
                            <div class="w_left w_25">
                                <span></span>
                            </div>
                            <div class="w_center w_65">
                                <div class="container">
                                    <canvas id="devices" width="300" height="300"></canvas>
                                </div>
                            </div>
                            <div class="w_right w_20">
                                <span><?php //$manufacturer['total']; ?></span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php //endforeach; ?>
	                </div> <!-- /content -->
              	</div> <!-- /panel -->
            </div> <!-- /col -->
        </div><!-- /row -->

        <div class="row">

            <!--All users; what percentage of registered users are place owners or normal user-->
            <div class="col-md-4 col-xs-12">
                <div class="x_panel tile fixed_height_320">
                    <div class="x_title">
                        <h2>All Users: Normal and Place Owners</h2>
                        <div class="clearfix"></div>
                    </div>
                    <br><br>
                    <div class="x_content">
                        <div class="widget_summary">
                            <div class="w_left w_15">
                                <span></span>
                            </div>
                            <div class="w_center w_65">
                                <div class="container">
                                    <canvas id="user_ratio" width="300" height="300"></canvas>
                                </div>
                            </div>
                           <div class="clearfix"></div>
                        </div>
                        <?php //endforeach; ?>
                    </div> <!-- /content -->
                </div> <!-- /panel -->
            </div>


            <!--Normal User Chart; Frequency of Registration in the last one year-->
            <div class="col-md-4 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Normal User Registration Chart</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a></li>
                                    <li><a href="#">Settings 2</a></li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <canvas id="mybarChart"></canvas>
                    </div>
                </div> <!-- /panel -->
            </div>

            <!--Chart of purchases made so far, pending payment to approved payment ratio-->
            <div class="col-md-4 col-xs-12">
                <div class="x_panel tile fixed_height_320">
                    <div class="x_title">
                        <h2>All Users: Normal and Place Owners</h2>
                        <div class="clearfix"></div>
                    </div>
                    <br><br>
                    <div class="x_content">
                        <div class="widget_summary">
                            <div class="w_left w_15">
                                <span></span>
                            </div>
                            <div class="w_center w_65">
                                <div class="container">
                                    <canvas id="purchase_ratio" width="300" height="300"></canvas>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php //endforeach; ?>
                    </div> <!-- /content -->
                </div> <!-- /panel -->
            </div>
        </div><!-- /row -->

    </div>
</div> <!-- /.col-right -->
<!-- /page content -->

<!--Import D3.js plugin-->
<script src="https://d3js.org/d3.v5.min.js"></script>

<!--Import chart.js plugin-->
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<!--Dashboard script file-->
<script src="<?php echo base_url('public/js/dashboard.js') ?>"></script>

<?php include "layout/footer.php";  //$this->load->view('admin/partials/admin_footer'); ?>

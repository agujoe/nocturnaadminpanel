 <div class="right_col" role="main">
        <h2 style="margin-top:0px">Notification <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">ID <?php echo form_error('userID') ?></label>
            <input type="text" class="form-control" name="userID" id="userID" placeholder="UserID" value="<?php echo $userID; ?>" />
        </div>
	    <!-- <div class="form-group">
            <label for="int">FriendID <?php //echo form_error('friendID') ?></label>
            <input type="text" class="form-control" name="friendID" id="friendID" placeholder="FriendID" value="<?php //echo $friendID; ?>" />
        </div> -->
	    <div class="form-group">
            <label for="varchar">Text <?php echo form_error('text') ?></label>
            <input type="text" class="form-control" name="text" id="text" placeholder="Text" value="<?php echo $text; ?>" />
        </div>
	   <!--  <div class="form-group">
            <label for="varchar">NotificationType <?php //echo form_error('notificationType') ?></label>
            <input type="text" class="form-control" name="notificationType" id="notificationType" placeholder="NotificationType" value="<?php //echo $notificationType; ?>" />
        </div> -->
	   <!--  <div class="form-group">
            <label for="int">IsRead <?php //echo form_error('isRead') ?></label>
            <input type="text" class="form-control" name="isRead" id="isRead" placeholder="IsRead" value="<?php echo $isRead; ?>" />
        </div> -->
	    <div class="form-group">
            <label for="varchar">Date <?php echo form_error('date') ?></label>
            <input type="text" class="form-control" name="date" id="date" placeholder="Date" value="<?php echo $date; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('notification') ?>" class="btn btn-default">Cancel</a>
	</form>
    </div>
 <div class="right_col" role="main">
     <h2 style="margin-top:0px">Place Owner <?php echo $button ?></h2>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
         <div class="form-group">
             <label for="varchar">UserName <?php echo form_error('userName') ?></label>
             <input type="text" class="form-control" name="userName" id="userName" placeholder="UserName" value="<?php echo $userName; ?>" />
         </div>

         <!--   <div class="form-group">
            <label for="varchar">NameOfClub <?php //echo form_error('nameOfClub') ?></label>
            <input type="text" class="form-control" name="nameOfClub" id="nameOfClub" placeholder="NameOfClub" value="<?php //echo $nameOfClub; ?>" />
         </div> -->

         <!--  <div class="form-group">
            <label for="varchar">StartTime <?php //echo form_error('startTime') ?></label>
            <input type="text" class="form-control" name="startTime" id="startTime" placeholder="StartTime" value="<?php //echo $startTime; ?>" />
         </div>

	     <div class="form-group">
	        <label for="varchar">EndTime <?php //echo form_error('endTime') ?></label>
            <input type="text" class="form-control" name="endTime" id="endTime" placeholder="EndTime" value="<?php //echo $endTime; ?>" />
         </div> -->

         <!--  <div class="form-group">
            <label for="varchar">ClubDis <?php //echo form_error('clubDis') ?></label>
            <input type="text" class="form-control" name="clubDis" id="clubDis" placeholder="ClubDis" value="<?php //echo $clubDis; ?>" />
         </div> -->

         <!--  <div class="form-group">
            <label for="varchar">Password <?php //echo form_error('password') ?></label>
            <input type="text" class="form-control" name="password" id="password" placeholder="Password" value="<?php //echo $password; ?>" />
         </div> -->

         <!--   <div class="form-group">
             <label for="varchar">PhoneNumber <?php //echo form_error('phoneNumber') ?></label>
             <input type="text" class="form-control" name="phoneNumber" id="phoneNumber" placeholder="PhoneNumber" value="<?php //echo $phoneNumber; ?>" />
         </div>

	     <div class="form-group">
             <label for="varchar">Address <?php //echo form_error('address') ?></label>
             <input type="text" class="form-control" name="address" id="address" placeholder="Address" value="<?php //echo $address; ?>" />
         </div> -->

         <div class="form-group">
             <label for="varchar">Email <?php echo form_error('email') ?></label>
             <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
         </div>

         <!-- <div class="form-group">
             <label for="varchar">DeviceToken <?php //echo form_error('deviceToken') ?></label>
             <input type="text" class="form-control" name="deviceToken" id="deviceToken" placeholder="DeviceToken" value="<?php //echo $deviceToken; ?>" />
         </div>

	     <div class="form-group">
             <label for="varchar">ClubType <?php //echo form_error('clubType') ?></label>
             <input type="text" class="form-control" name="clubType" id="clubType" placeholder="ClubType" value="<?php //echo $clubType; ?>" />
         </div>
	     <div class="form-group">
             <label for="varchar">TypeDevice <?php //echo form_error('typeDevice') ?></label>
             <input type="text" class="form-control" name="typeDevice" id="typeDevice" placeholder="TypeDevice" value="<?php //echo $typeDevice; ?>" />
         </div> -->

         <!--  <div class="form-group">
             <label for="profile">Profile <?php //echo form_error('profile') ?></label>
             <textarea class="form-control" rows="3" name="profile" id="profile" placeholder="Profile" <?php //echo $profile; ?></textarea>
         </div> -->


         <?php
         //When create
         if($button=="Create")
         {
             ?>

             <div class="form-group">
                 <label for="profile">Profile<?php echo form_error('profile') ?></label>
                 <input type="file" class="form-control" rows="3" name="profile" id="profile" placeholder="Profile" value="<?php echo $profile; ?>" />
             </div>

         <?php
         }
         //when update
         else if($button=="Update")
         {
             ?>

             <div class="form-group">
                 <label for="profile">Profile<?php echo form_error('profile') ?></label>
                 <input type="file" class="form-control" rows="3" name="profile" id="profile" /><br>
                 <img src="<?php echo $image_url.$profile; ?>" id="showImg" width="200px !important" height="100px !important" />

             </div>

             <script>
                 function readURL(input) {
                     if (input.files && input.files[0]) {
                         var reader = new FileReader();

                         reader.onload = function (e) {
                             $('#showImg').attr('src', e.target.result);
                         };

                         reader.readAsDataURL(input.files[0]);
                     }
                 }

                 $("#profile").change(function(){
                     readURL(this);
                 });

             </script>

             <?php
         }

         ?>

         <!--  <div class="form-group">
            <label for="cover">Cover <?php //echo form_error('cover') ?></label>
            <textarea class="form-control" rows="3" name="cover" id="cover" placeholder="Cover"><?php //echo $cover; ?></textarea>
         </div>

	     <div class="form-group">
             <label for="varchar">Lat <?php //echo form_error('lat') ?></label>
             <input type="text" class="form-control" name="lat" id="lat" placeholder="Lat" value="<?php //echo $lat; ?>" />
         </div>

	     <div class="form-group">
             <label for="varchar">Long <?php //echo form_error('long') ?></label>
             <input type="text" class="form-control" name="long" id="long" placeholder="Long" value="<?php //echo $long; ?>" />
         </div> -->

         <input type="hidden" name="id" value="<?php echo $id; ?>" />
         <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
	     <a href="<?php echo site_url('owneruser') ?>" class="btn btn-default">Cancel</a>
     </form>
 </div>
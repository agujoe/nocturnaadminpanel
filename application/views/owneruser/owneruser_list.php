 <div class="right_col" role="main">
     <h2 style="margin-top:0px">Place Owner</h2>
     <div class="row" style="margin-bottom: 10px">
         <div class="col-md-4">
             <?php echo anchor(site_url('owneruser/create'),'Create', 'class="btn btn-primary"'); ?>
         </div>

         <div class="col-md-4 text-center">
             <div style="margin-top: 8px" id="message">
                 <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
             </div>
         </div>

         <div class="col-md-1 text-right"></div>
         <div class="col-md-3 text-right">
             <form action="<?php echo site_url('owneruser/index'); ?>" class="form-inline" method="get">
                 <div class="input-group">
                     <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                     <span class="input-group-btn">
                         <?php
                         if ($q <> '')
                         {
                             ?>
                             <a href="<?php echo site_url('owneruser'); ?>" class="btn btn-default">Reset</a>
                             <?php
                         }
                         ?>
                         <button class="btn btn-primary" type="submit">Search</button>
                     </span>
                 </div>
             </form>
         </div>
     </div>

     <table class="table table-bordered" style="margin-bottom: 10px">
         <tr style="background-color: #4c177d; color:#ffffff;">
             <th>No</th>
             <th>UserName</th>
             <!-- <th>NameOfClub</th> -->
             <!--<th>StartTime</th>
             th>EndTime</th> -->
             <!-- <th>ClubDis</th> -->
             <th>Password</th>
             <th>PhoneNumber</th>
             <th>Address</th>
             <th>Email</th>
             <!-- <th>DeviceToken</th>
             <th>ClubType</th>
             <th>Device</th> -->
<!--             <th>ProfilePicture</th>-->
             <!-- <th>Cover</th>
             <th>Lat</th>
             <th>Long</th> -->
             <th style="width:14% !important;">Action</th>
         </tr>
         <?php
         foreach ($owneruser_data as $owneruser)
         {
             ?>

             <tr>
                 <td width="80px"><?php echo ++$start ?></td>
                 <td><?php echo $owneruser->userName ?></td>
                 <!--<td><?php //echo $owneruser->nameOfClub ?></td>-->
                 <!--<td><?php //echo $owneruser->startTime ?></td>
                 <!--<td><?php //echo $owneruser->endTime ?></td>
                 <!--<td><?php //echo $owneruser->clubDis ?></td>-->
                 <td><?php echo $owneruser->password ?></td>
                 <td><?php echo $owneruser->phoneNumber ?></td>
                 <td><?php echo $owneruser->address ?></td>
                 <td><?php echo $owneruser->email ?></td>
                 <!--<td><?php //echo $owneruser->deviceToken ?></td>-->
<!--                 <td><?php//*/ echo $owneruser->clubType */?></td>
-->                 <!--<td><?php //echo $owneruser->typeDevice ?></td>-->
<!--                 <td><?php /*//echo $owneruser->profile */?><img src="<?php /*echo $image_url.$owneruser->profile; */?>" width="200px" height="100px" onerror="this.src = ''http://nocturnaapp.com/adminpanel/noimage.png';"></td>
-->                 <!--<td><?php //echo $owneruser->cover ?></td>
                 <td><?php //echo $owneruser->lat ?></td>
                 <td><?php //echo $owneruser->long ?></td>-->

                 <td>
                     <div class="col-md-12">
                         <div class="btn-group">
                             <a href="<?php echo base_url('owneruser/read/'.$owneruser->id);?>"><button class="btn btn-sm btn-success" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Read"><i class="fa fa-book"></i></button></a>
                             <a href="<?php echo base_url('owneruser/update/'.$owneruser->id);?>"><button class="btn btn-sm btn-primary" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Update"><i class="fa fa-edit"></i></button></a>
                             <a href="<?php echo base_url('owneruser/delete/'.$owneruser->id);?>"><button class="btn btn-sm btn-danger" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></button></a>
                         </div>
                     </div>
                 </td>
             </tr>
             <?php
         }
         ?>
     </table>

     <div class="row">
         <div class="col-md-6">
             <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
         </div>

         <div class="col-md-6 text-right">
             <?php echo $pagination ?>
         </div>
     </div>
 </div>
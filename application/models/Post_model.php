<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Post_model extends CI_Model
{

    public $table = 'post';
    public $id = 'id';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    // get all
    public function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    public function total_rows($q = NULL) {
        $this->db->like('id', $q);
        $this->db->or_like('userID', $q);
        $this->db->or_like('picture', $q);
        $this->db->or_like('status', $q);
        $this->db->or_like('checkIn', $q);
        $this->db->or_like('video', $q);
        $this->db->or_like('statusType', $q);
        $this->db->or_like('date', $q);
        $this->db->from($this->table);

        return $this->db->count_all_results();
    }

    // get data with limit and search
    public function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
	    $this->db->or_like('userID', $q);
	    $this->db->or_like('picture', $q);
	    $this->db->or_like('status', $q);
	    $this->db->or_like('checkIn', $q);
	    $this->db->or_like('video', $q);
	    $this->db->or_like('statusType', $q);
	    $this->db->or_like('date', $q);
	    $this->db->limit($limit, $start);

	    return $this->db->get($this->table)->result();
    }

    // insert data
    public function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    public function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    /***
     * Fetch post data per month
     */
    public function getPostPerMonth()
    {
        //All months in the last one year
        $all_months = 12;
       // $month_details = [];
        //$post_per_month = [];

        $now = date("Y-m-d H:i:s");
        for($x = 12; $x >= 0; $x--){
            $month = date('Y-m-d H:i:s', strtotime($now . " - $x month"));
            $a_month_ago = date('Y-m-d H:i:s', strtotime($month. "- 1 month"));

            $month_details[] = $month;
            //get post between this diff
            $sql = "select count(*) as post_count from post where create_date between '$a_month_ago' and  '$month'";

            $query_result = $this->db->query($sql)->result_array()[0]['post_count'];

            $post_per_month[] = $query_result;

            $joint_result = array_combine($month_details, $post_per_month);
        }

        return $joint_result;
    }
}
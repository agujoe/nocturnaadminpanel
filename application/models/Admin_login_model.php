<?php

Class Admin_login_model extends CI_Model {

    public function login($username, $password) {
        $this->db->select('name, password');
        $this->db->from('admin_login');
        $this->db->where(['name' => $username, 'password' => $password]);
        $query = $this->db->get()->result();

        if (!is_null($query) && count($query) > 0) {
            return $query[0];
        }
    }

    public function checkname($username){
        $q = "SELECT count(*) as name from admin_login where name !='$username'";
        return $this->db->query($q)->row_array();
    }

    public function checkpassword($password){
        $q = "SELECT count(*) as password from admin_login where password !='$password'";
        return $this->db->query($q)->row_array();
    }


    /*** The function takes in the table name and
     * returns the count of users in it
     */
    public function countRowInTable($table_name){
        $q = "SELECT count(*) as normaluser from $table_name";
        $result = $this->db->query($q)->result_array();
        return $result[0]['normaluser'];
    }

    /** Dashboard analysis data requirement
     * It fetches all data for appropriate analysis on the dashboard
     */

    public function allNormalUser(){
        $normalUsers = $this->countRowInTable('normaluser');
        return $normalUsers;
    }

    public function allPlaceOwner(){
        $placeOwners = $this->countRowInTable('owneruser');
        return $placeOwners;
    }

    public function allCartItem(){
        $cartItem = $this->countRowInTable('cart');
        return $cartItem;
    }

    public function allPackage(){
        $package = $this->countRowInTable('package');
        return $package;
    }


    public function allPost(){
        $postItem = $this->countRowInTable('post');
        return $postItem;
    }

    public function allPurchaseItem(){
        $purchaseItem = $this->countRowInTable('purchased');
        return $purchaseItem;
    }

    public function  getDeviceRatio(){
        $this->db->where('typeDevice' , 'android');
        $this->db->from('normaluser');
        $result_android = $this->db->count_all_results();

        $this->db->where('typeDevice', 'ios');
        $this->db->from('normaluser');
        $result_ios = $this->db->count_all_results();

        return [
            'android' => $result_android,
            'ios' => $result_ios
        ];
    }


    public function getNormalUserAgainstPlaceOwners()
    {

        $q = "SELECT count(*) from normaluser";
        $normaluser = $this->db->query($q)->row_array();
        $normaluser_count = $normaluser["count(*)"];


        $q = "SELECT count(*) from owneruser";
        $owneruser = $this->db->query($q)->row_array();
        $owneruser_count = $owneruser["count(*)"];


        return [
            'normaluser' => $normaluser_count,
            'owneruser' => $owneruser_count
        ];
    }

    public function getPaymentStatusRatio()
    {
        $this->db->where('status' , 'pending');
        $this->db->from('cart');
        $result_pending = $this->db->count_all_results();

        $this->db->where('status', 'purchased');
        $this->db->from('cart');
        $result_purchased = $this->db->count_all_results();

        return [
            'pending' => $result_pending,
            'purchased' => $result_purchased
        ];
    }
}

?>


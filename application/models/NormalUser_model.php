<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class NormalUser_model extends CI_Model
{
    public $table = 'normaluser';
    public $id = 'id';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    // get all
    public function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    public function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    public function total_rows($q = NULL) {
        $this->db->like('id', $q);
        $this->db->or_like('googlePlus', $q);
        $this->db->or_like('faceBook', $q);
        $this->db->or_like('userName', $q);
        $this->db->or_like('fullName', $q);
        $this->db->or_like('age', $q);
        $this->db->or_like('gender', $q);
        $this->db->or_like('city', $q);
	    $this->db->or_like('email', $q);
	    $this->db->or_like('password', $q);
	    $this->db->or_like('phoneNumber', $q);
	    $this->db->or_like('intrest', $q);
	    $this->db->or_like('dis', $q);
	    $this->db->or_like('deviceToken', $q);
	    $this->db->or_like('typeDevice', $q);
	    $this->db->or_like('profilePicture', $q);
	    $this->db->from($this->table);

	    return $this->db->count_all_results();
    }

    // get data with limit and search
    public function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
	    $this->db->or_like('googlePlus', $q);
	    $this->db->or_like('faceBook', $q);
	    $this->db->or_like('userName', $q);
	    $this->db->or_like('fullName', $q);
	    $this->db->or_like('age', $q);
	    $this->db->or_like('gender', $q);
	    $this->db->or_like('city', $q);
	    $this->db->or_like('email', $q);
	    $this->db->or_like('password', $q);
	    $this->db->or_like('phoneNumber', $q);
	    $this->db->or_like('intrest', $q);
	    $this->db->or_like('dis', $q);
	    $this->db->or_like('deviceToken', $q);
	    $this->db->or_like('typeDevice', $q);
	    $this->db->or_like('profilePicture', $q);
	    $this->db->limit($limit, $start);

	    return $this->db->get($this->table)->result();
    }

    // insert data
    public function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    //update data
    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    public function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}